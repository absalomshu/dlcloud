<%@ page import="javax.xml.parsers.*,org.w3c.dom.*" %>


<%
try{
	//String XmlPath = "speechSynthesis.xml";
	String App = request.getParameter("app");
	String XmlPath = App + ".xml";
	//System.out.println(App);
%>
<%!
	Document doc;
	String getXMLValue(String name) {
		NodeList nlist=doc.getElementsByTagName(name);
		String value = nlist.item(0).getFirstChild().getNodeValue();
		return value;
	}
%>
<%
// appPath value should be the directory in which the xml file is kept - 
String appPath = application.getRealPath("/")+"/configurations/";
DocumentBuilderFactory dbf=DocumentBuilderFactory.newInstance();
DocumentBuilder db=dbf.newDocumentBuilder();
doc=db.parse(appPath + XmlPath);


String Title    = getXMLValue("title");
String Description    = getXMLValue("description");
String InputType    = getXMLValue("inputType");
String AllowedInputExtensions    = getXMLValue("allowedInputExtensions");
String OutputType    = getXMLValue("outputType");
String OutputExtensions    = getXMLValue("outputExtensions");
String host    = getXMLValue("host");
String port    = getXMLValue("port");
String name    = getXMLValue("name");
String intermediateFile    = getXMLValue("intermediateFile");
String serviceStartCommand    = getXMLValue("serviceStartCommand");
//System.out.println(port);
%>
<%@ include file="workarea.html" %>
<%
}catch(Exception e){
out.print(e);
}%>

