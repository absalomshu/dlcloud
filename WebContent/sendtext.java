import java.net.*;
import java.io.*;
import java.nio.*;
import java.util.Arrays;

public class sendtext {

	public static void main(String[] args){

		try {
			Socket serviceSocket = null;
			DataOutputStream outputStream = null;
			DataInputStream inputStream = null;

			//initialize input and output streams
			serviceSocket = new Socket("163.143.88.162", 10000);
			outputStream = new DataOutputStream(serviceSocket.getOutputStream());
			inputStream = new DataInputStream(serviceSocket.getInputStream());

			//if all is ok, write data to socket
			if(serviceSocket != null && outputStream != null && inputStream != null){
				try{
					try{
						
						File myFile = new File("client.txt");
						BufferedInputStream in = new BufferedInputStream(new FileInputStream(myFile));
						int size = in.available();
						int count;
						
						byte inputFileSize = (byte) myFile.length();
						byte[] data = new byte[(int) myFile.length()];
						//byte[] buffer = new byte[1024];
						
						
						//header
						byte[] header = {1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,inputFileSize,0,0,0};
					
						//write the header, then the text, then flush/send
						outputStream.write(header);						
						while ((count = in.read(data)) > 0) {
							 outputStream.write(data, 0, count);
						}
						outputStream.flush();
						
						
						//server response
						//Create 20 digit array for response header, plus lenght of inputFile
						int responseSize = 20 + inputFileSize;
						int[] response = new int[responseSize];
						int i=0;

						while(i<responseSize){
							//System.out.println(inputStream.read());
							response[i]=inputStream.read();
							i++;
						}
						
						
						System.out.println("Header sent:" + Arrays.toString(header));
						//System.out.println("Data sent:" + inputText);
						System.out.println("Data sent(bytes):" + Arrays.toString(data));
						System.out.println("Server response: " + Arrays.toString(response));
				 
						
					}
					 catch (FileNotFoundException ex) {
						System.out.println("File not found. ");
					}
					
					inputStream.close();
					outputStream.close();
					serviceSocket.close();
					
				}catch (IOException e) {
					//System.out.println(e);
					e.printStackTrace();
				}
			}

		}
		catch (IOException e) {
		System.out.println(e);
		//e.printStackTrace();
		}
	}
}
