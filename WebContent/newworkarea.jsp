<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bootstrap 3, from LayoutIt!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/dropzone.css" rel="stylesheet">

<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
	<script src="js/dropzone.js"></script>
	<script src="js/jquery-form.min.js"></script>


  </head>
  <body>
 
  
  
  <div id="loading" style="display:none; color:black;">
  	  <h3>Processing</h3>
	  <img id="loading-image" src="images/loading.gif" alt="Loading..." />
	</div>
  <!-- <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">University of Aizu</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div><!--/.navbar-collapse 
      </div>
    </nav>-->

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron" style="padding-top: 0px;">
      <div class="container-fluid">
	      <div class="row">
	      	<div class="col-md-8">
		        <h2>${requestScope.name}</h2>
		        <p>${requestScope.description}</p>
		        <form id="stop-service" action="StopService" method="GET">
		       		 <input name="host" type="hidden" value="${requestScope.host}" >
					 <input name="port" type="hidden" value="${requestScope.port}">
										
		         	<p>
		         	<a href="#" class="btn btn-primary" id="dummy-start-processing"><span class="glyphicon glyphicon-play"></span> Start processing</a>
		         	<button type="DONTsubmit" class="btn btn-warning" href="#" role="button" id="stop-service-button"><span class="glyphicon glyphicon-stop"></span> Stop service</button>
		         		
		         	</p>
		         </form>
		         
		         
		        <!--<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>-->
		        
	        </div>
	     
	      
	      	<div class="col-md-4 text-right" id="intermediateFile">
		      <div  class="pull-right">
		      	<% 
				//Only show if it's set in config file
				final String intermediateFile = (String) request.getAttribute ("intermediateFile");
				if(!intermediateFile.equals("")){ %>
					<!--<h5 style="font-weight:normal;">Intermediate image</h5>--><br/>
					        <!-- <p><img src="images/${requestScope.intermediateFile}"  alt="Pulpit rock"  height="100"/></p> -->
					       
					       <img id="int-image-preview" src="#"  style="display:none;" class="img-thumbnail preview" />
					       
					       <input type="file" id="int-file-input" name="intermediateFile"   accept="image/*" class="btn btn-block btn-default btn-lg" style="display:none;"/> 
					       <a name="dummy-int-image" onclick="document.getElementById('int-file-input').click();" class="btn btn-block btn-default input-file"/>Choose style image</a>	<br/>
								
					       
					       <!-- A hidden input to specify whether an intermediate file is required or not -->
					       <input type="hidden" name="intermediateFileRequired" id="int-file-required" value="1"/>
				<% } %>
		      	
		       </div>
	        </div>
	        
	       </div>
      </div>
    </div>
	<% 
	final String inputType = (String) request.getAttribute ("inputType");
	if(inputType.equals("image")){ %>
		<%@ include file="imageWorkarea.html"%>
	<% } else { %>
		<%@ include file="textWorkarea.html"%>
	<% }	%>
				<!-- This form is used to restart the service if the page if reloaded -->
				<form action="StartService" method="POST" id="start-service">
					<button type="submit" class="btn btn-block btn-primary select-srvc-btn" name="serviceSelected" value="${requestScope.idOfServiceSelected}" style="display:none;">
						Select
					</button>
				</form>	




    
	
	<script type="text/javascript">
	
	/*
	$("#add-file").click(function(){
		//alert('ere');
		$("#input-rows").append('<div class="input-row "><div class="col-md-10"><input type="file" class="btn btn-block btn-default btn-lg ">  </div><div class="col-md-2"><input type="submit" value="Cancel" class="btn btn-block  btn-lg"/>  </div></div>');
	}); */
	/*
	$(document).ready(function() {
	    var max_fields      = 10; //maximum input boxes allowed
	    var wrapper         = $("#input-rows"); //Fields wrapper
	    var add_button      = $("#add-file"); //Add button ID
	    
	    var x = 1; //initlal text box count
	    $(add_button).click(function(e){ //on add input button click
	        e.preventDefault();
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment
	           // $(wrapper).append('<div><input type="text" name="mytext[]"/><a href="#" class="remove_field">Remove</a></div>'); //add input box
	        	$(wrapper).append('<div class="input-row "><div class="col-md-10"><input type="file" name="inputFile[]" class="btn btn-block btn-default btn-lg ">  </div><div class="col-md-2"><button value="Cancel" class="btn btn-lg cancel">Cancel</button>  </div></div>');
		
			}
	    });
	    
	    $(wrapper).on("click",".cancel", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
	    })
	}); */
	
	//Display image thumbnail before upload
	function intermediateImagePreview(input) {

	    if (input.files && input.files[0]) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	        	//$(".myinput").prepend('<img id="imagePreview1" src="#"  alt="Pulpit rock" width="304" height="228"/>')
	            
	        	$('#int-image-preview').show();
	        	$('#int-image-preview').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$("#int-file-input").change(function(){
		intermediateImagePreview(this);
	});
	
	
	
	
	
	
	
	
	/*SEND SINGLE FILE VIA SERVLET 3.0
	$("form#sendfiles").submit(function(){
	//var url = "http://localhost:8080/dlcloud/UploadFile.jsp?host="+'${requestScope.host}'+"&port="+'${requestScope.port}'+"&inputType="+'${requestScope.inputType}';
	
	var url = "http://localhost:8080/dlcloud/UploadServlet";
	var formData = new FormData();
	//alert(JSON.stringify(formData));
	formData.append("file", document.getElementById("myinput").files[0]);
	
	var xhr = new XMLHttpRequest();
	xhr.open("POST", "UploadServlet");
	xhr.send(formData);
	 $("#result1").prepend('<img id="resultImage" src="results/works.jpg"  alt="Pulpit rock" width="304" height="228"/>')
	   return false; 
	});
	 */
	
	// $(window).load(function() {
	     //$('#loading').hide();
		 //$("#testarea").load("results/test.txt");
		 //$.get( "C:/Users/Absalom/Dropbox/Java/test.txt", function( data ) {
			//  alert( "Sata" + data );
			//});
		// alert('here');
	 // });	 
	

	 
	 
	
	
</script>
	
	
	
	
	
	
<script type="text/javascript">
	/*Dropzone.options.fileDropzone = {
	 // paramName: "uploadedfile", // The name that will be used to transfer the file
	  maxFilesize: 2, // MB
	  autoProcessQueue: false,
	 // autoProcessQueue: false,	 // Prevents Dropzone from uploading dropped files immediately
	 addRemoveLinks: "dictRemoveFile ",
	  //acceptedFiles: ".txt ",
	 /* accept: function(file, done) {
		if (file.name == "justinbieber.jpg") {
		  done("Naha, you don't.");
		}
		else { done(); }
	  }
	};  */
	
	
	/*
	
	
	
	Dropzone.options.fileDropzone = {

  // Prevents Dropzone from uploading dropped files immediately
  autoProcessQueue: false,
  maxFilesize: 2, // MB
  addRemoveLinks: "dictRemoveFile ",
  acceptedFiles: ".txt ",
  
	
  init: function() {
    var submitButton = document.querySelector("#submit-files")
        fileDropzone = this; // closure

    submitButton.addEventListener("click", function() {
      fileDropzone.processQueue(); // Tell Dropzone to process all queued files.
	  location.reload();
   });

    // You might want to show the submit button only when 
    // files are dropped here:
    this.on("addedfile", function() {
      // Show submit button here and/or inform user to click it.
    });

  }
}; 	

Dropzone.options.imageDropzone = {

  // Prevents Dropzone from uploading dropped files immediately
  autoProcessQueue: false,
  addRemoveLinks: "dictRemoveFile ",
  acceptedFiles: ".JPG,.PNG,.JPEG,.IMG ",
	
  init: function() {
    var submitButton = document.querySelector("#submit-images")
        imageDropzone = this; // closure

    submitButton.addEventListener("click", function() {
      imageDropzone.processQueue(); // Tell Dropzone to process all queued files.
	  location.reload();
   });

    // You might want to show the submit button only when 
    // files are dropped here:
    this.on("addedfile", function() {
      // Show submit button here and/or inform user to click it.
    });

  }
}; */
	</script>
  </body>
</html>