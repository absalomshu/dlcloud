<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bootstrap 3, from LayoutIt!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/dropzone.css" rel="stylesheet">

  </head>
  <body>
	 

	
    <div class="container-fluid" style="margin-top:25%;">
    <%
    
    //Display notice if there is.
    if(null != session.getAttribute("notice")){
    	out.println("<div class='alert alert-danger'><a href='#' class='close' data-dismiss='alert'>&times;</a>");  
    	out.println(session.getAttribute("notice"));
    	out.println("</div>");
    	session.removeAttribute("notice");
    }
%>
    
	
</div>



  </body>
</html>