<%@ page import="java.io.*,java.util.*, javax.servlet.*, java.nio.*, java.net.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.output.*" %>

<%
  
   File file ;
   int maxFileSize = 5000 * 1024;
   int maxMemSize = 5000 * 1024;
   ServletContext context = pageContext.getServletContext();
   String filePath = context.getInitParameter("file-upload");

   // Verify the content type
   String contentType = request.getContentType();
   if ((contentType.indexOf("multipart/form-data") >= 0)) {

      DiskFileItemFactory factory = new DiskFileItemFactory();
      // maximum size that will be stored in memory
      factory.setSizeThreshold(maxMemSize);
      // Location to save data that is larger than maxMemSize.
      factory.setRepository(new File("c:\\temp"));

      // Create a new file upload handler
      ServletFileUpload upload = new ServletFileUpload(factory);
      // maximum file size to be uploaded.
      upload.setSizeMax( maxFileSize );
      try{ 
         // Parse the request to get file items.
         List fileItems = upload.parseRequest(request);

         // Process the uploaded file items
         Iterator i = fileItems.iterator();

         out.println("<html>");
         out.println("<head>");
         out.println("<title>JSP File upload</title>");  
         out.println("</head>");
         out.println("<body>");
         while ( i.hasNext () ) 
         {
            FileItem fi = (FileItem)i.next();
            if ( !fi.isFormField () )	
            {
            // Get the uploaded file parameters
            String fieldName = fi.getFieldName();
            String fileName = fi.getName();
            boolean isInMemory = fi.isInMemory();
            long sizeInBytes = fi.getSize();
            // Write the file
            if( fileName.lastIndexOf("\\") >= 0 ){
            file = new File( filePath + 
            fileName.substring( fileName.lastIndexOf("\\"))) ;
            }else{
            file = new File( filePath + 
            fileName.substring(fileName.lastIndexOf("\\")+1)) ;
            }
            fi.write( file ) ;
            out.println("Uploaded Filename: " + filePath + 
            fileName + "<br>");
            }
         }
		
		
         out.println("</body>");
         out.println("</html>");
      }catch(Exception ex) {
         System.out.println(ex);
      } 
//Socket connection
		try {
			Socket serviceSocket = null;
			DataOutputStream outputStream = null;
			DataInputStream inputStream = null;

			//initialize input and output streams
			serviceSocket = new Socket("163.143.88.162", 10000);
			outputStream = new DataOutputStream(serviceSocket.getOutputStream());
			inputStream = new DataInputStream(serviceSocket.getInputStream());



			//if all is ok, write data to socket
			if(serviceSocket != null && outputStream != null && inputStream != null){
				try{
					//Test with dataOUtputstream
					/*int[] digits = new int[20];
					DataOutputStream out = new DataOutputStream(
					new BufferedOutputStream(serviceSocket.getOutputStream()));
					*/
				
					//byte[] message = new byte[20];
					
					
					//data
					String inputText = "Is this ok?";
				
					//store text as bytes in data byte array
					//for some reason, 2 bytes are lost, so I manually add so the complete text is sent
					byte[] data = inputText.getBytes();	
					int dataLength = 2 + data.length;
						
					//header
					byte[] header = {1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,(byte) dataLength,0,0,0};
					//int headerLength = 20;
					
					
					//write the header, then the text, then flush/send
					outputStream.write(header);
					outputStream.writeUTF(inputText);
					outputStream.flush();
					
					//server response
					//Create 20 digit array for response
					int[] serverResponse = new int[20];
					int i=0;
					
					while(i<20){
						//System.out.println(inputStream.read());
						serverResponse[i]=inputStream.read();
						i++;
					}
					
					//System.out.println(header.length);
					System.out.println("Header sent:" + Arrays.toString(header));
					System.out.println("Data sent:" + inputText);
					System.out.println("Data sent(bytes):" + Arrays.toString(data));
					System.out.println("Server response: " + Arrays.toString(serverResponse));
				 
					//inputStream.close();
					//outputStream.close();
					//serviceSocket.close();
					
					//String str = "TEXT";
					//byte[] bytes = str.getBytes();
					//System.out.println(Arrays.toString(bytes));
					//outputStream.write(bytes);


					//outputStream.writeBytes(bobj);
					//inputStream.write(bobj.length);
					//outputStream.write(bobj);
					//final byte[] bytes = outputStream.toByteArray();
					//dos.writeInt(5);
				}catch (IOException e) {
					//System.out.println(e);
					e.printStackTrace();
				}
			}

		}
		catch (IOException e) {
		System.out.println(e);
		//e.printStackTrace();
		}
		 	  
   }else{
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet upload</title>");  
      out.println("</head>");
      out.println("<body>");
      out.println("<p>No file uploaded</p>"); 
      out.println("</body>");
      out.println("</html>");
   }
%>