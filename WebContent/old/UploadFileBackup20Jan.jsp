<%@ page import="java.io.*,java.util.*, javax.servlet.*, java.nio.*, java.net.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>
<%@ page import="org.apache.commons.io.output.*" %>

<%
  
   File file ;
	String host = request.getParameter("host");
	int port = Integer.parseInt(request.getParameter("port"));
	System.out.println(port);
	System.out.println(host);

   int maxFileSize = 5000 * 1024;
   int maxMemSize = 5000 * 1024;
   ServletContext context = pageContext.getServletContext();
   String filePath = context.getInitParameter("file-upload");

   // Verify the content type
   String contentType = request.getContentType();
   if ((contentType.indexOf("multipart/form-data") >= 0)) {

      DiskFileItemFactory factory = new DiskFileItemFactory();
      // maximum size that will be stored in memory
      factory.setSizeThreshold(maxMemSize);
      // Location to save data that is larger than maxMemSize.
      factory.setRepository(new File("c:\\temp"));

      // Create a new file upload handler
      ServletFileUpload upload = new ServletFileUpload(factory);
      // maximum file size to be uploaded.
      upload.setSizeMax( maxFileSize );
      try{ 
         // Parse the request to get file items.
         List fileItems = upload.parseRequest(request);

         // Process the uploaded file items
         Iterator i = fileItems.iterator();

         out.println("<html>");
         out.println("<head>");
         out.println("<title>JSP File upload</title>");  
         out.println("</head>");
         out.println("<body>");
         while ( i.hasNext () ) 
         {
            FileItem fi = (FileItem)i.next();
            
            //String ifName;
            //String ifValue;
            
           // if ( fi.isFormField () ){
            	//If it's a form field, requet.getParameter() doesn't work with enctype=multipart/...
            	
            	  // ifName = fi.getFieldName();//text1
                   //ifValue = fi.getString();
                   //System.out.println(ifName+" : "+ ifValue);
                   //Put them in an array and use later.
            	 
           // }
            
            if ( !fi.isFormField () )	
            {	//If it's not a form field, it's a file. Treat it
				// Get the uploaded file parameters
				
				String fieldName = fi.getFieldName();
				String fileName = fi.getName();
				boolean isInMemory = fi.isInMemory();
				long sizeInBytes = fi.getSize();
				
				// Write the file
				if( fileName.lastIndexOf("\\") >= 0 ){
					file = new File( filePath + 
					fileName.substring( fileName.lastIndexOf("\\"))) ;
				}else{
					file = new File( filePath + 
					fileName.substring(fileName.lastIndexOf("\\")+1)) ;
				}
				fi.write( file ) ;
				out.println("Uploaded Filename: " + filePath + fileName + "<br>");
				
				
				
				try {
					Socket serviceSocket = null;
					DataOutputStream outputStream = null;
					DataInputStream inputStream = null;
		
					//initialize input and output streams
					//serviceSocket = new Socket("163.143.88.162", 10000);
					serviceSocket = new Socket(host, port);
					outputStream = new DataOutputStream(serviceSocket.getOutputStream());
					inputStream = new DataInputStream(serviceSocket.getInputStream());
		
					//if all is ok, write data to socket
					if(serviceSocket != null && outputStream != null && inputStream != null){
						try{
							try{
								
								//File myFile = new File("client.txt");
								File myFile = file;
								BufferedInputStream in = new BufferedInputStream(new FileInputStream(myFile));
								int size = in.available();
								int count;
								
								byte inputFileSize = (byte) myFile.length();
								byte[] data = new byte[(int) myFile.length()];
								//byte[] buffer = new byte[1024];
								
								
								//header
								byte[] header = {1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0,inputFileSize,0,0,0};
							
								//write the header, then the text, then flush/send
								outputStream.write(header);						
								while ((count = in.read(data)) > 0) {
									 outputStream.write(data, 0, count);
								}
								outputStream.flush();
								
								
								//server response
								//Create 20 digit array for response header, plus lenght of inputFile
								int responseSize = 20 + inputFileSize;
								byte[] serverResponse = new byte[responseSize];
								int j=0;
		
								while(j<responseSize){
									//Receive input byte by byte
									serverResponse[j]=(byte)inputStream.read();
									j++;
								}
								
								String serverResponseString = new String(serverResponse);
								
								System.out.println("Header sent:" + Arrays.toString(header));
								//System.out.println("Data sent:" + inputText);
								System.out.println("Data sent(bytes):" + Arrays.toString(data));
								System.out.println("Server response(bytes): " + Arrays.toString(serverResponse));
								System.out.println("Server response(bytes): " + serverResponseString);
								
							}
							 catch (FileNotFoundException ex) {
								System.out.println("File not found. ");
							}
							
							inputStream.close();
							outputStream.close();
							serviceSocket.close();
							
						}catch (IOException e) {
							//System.out.println(e);
							e.printStackTrace();
						}
					}
		
				}
				catch (IOException e) {
				System.out.println(e);
				//e.printStackTrace();
			}
				
				
				
            }
            
           
         }
		
		
         out.println("</body>");
         out.println("</html>");
      }catch(Exception ex) {
         System.out.println(ex);
      } 
//Socket connection
			
		 	  
   }else{
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet upload</title>");  
      out.println("</head>");
      out.println("<body>");
      out.println("<p>No file uploaded</p>"); 
      out.println("</body>");
      out.println("</html>");
   }
%>