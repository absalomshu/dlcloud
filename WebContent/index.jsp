<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "x" uri = "http://java.sun.com/jsp/jstl/xml" %>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bootstrap 3, from LayoutIt!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/dropzone.css" rel="stylesheet">

  </head>
  <body>
	  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="${pageContext.request.contextPath}/index.jsp">University of Aizu</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
         
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Deep Learning Cloud</h1>
        <p>Providing Deep Learning as a Cloud Service</p>
        <!--<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>-->
      </div>
    </div>
	
    <div class="container-fluid">
    <%
    
    //Display notice if there is.
    if(null != session.getAttribute("notice")){
    	out.println("<div class='alert alert-warning'><a href='#' class='close' data-dismiss='alert'>&times;</a>");  
    	out.println(session.getAttribute("notice"));
    	out.println("</div>");
    	session.removeAttribute("notice");
    }
%>
    
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
			  
			  <li class="active">Home</li>
			</ol>

				<form action="directory" method="GET" id="app-categories">
				
				
				<c:import var = "appCategories" url="configurations/app_categories.xml"/>
				<x:parse xml = "${appCategories}" var = "output"/>
			         
		         <x:forEach select = "$output/appCategories/category" var = "category">
		         
		         
		         	<div class="col-md-4 ">
						<div class=" panel panel-default">
							<div class="panel-body">
								<h2>
									<x:out select = "$category/title" />
								</h2>
								<p>
									<x:out select = "$category/description" />
									
								</p><br>
								 
									<button type="submit" class="btn btn-block btn-primary" name="category" value='<x:out select = "$category/type" />'> Select	</button>
								
							
							</div>
						</div>
					</div>
		      
		            
		         </x:forEach>
			      
				</form>
				
				
				<!-- <form action="manage" method="GET" >
					<div class="col-md-4 ">
					<button type="submit" class="btn " >Manage services	</button>
					</div>
				</form> -->
			
			<div id="demo"></div>
		
			<div class="row">
				<div class="col-md-12">
					University of Aizu, 2016.
				</div>
			</div>
			
		</div>
	</div>
</div>




    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
<script>

/*
LOAD CONFIG WITH JAVASCRIPT
var xhttp = new XMLHttpRequest();

xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
    	loadCategories(this);
    }
};
xhttp.open("GET", "configurations/categories.xml", true);
xhttp.send();

function loadCategories(xml) {
    var xmlDoc = xml.responseXML;
  
    var type =  xmlDoc.getElementsByTagName("type")[0].childNodes[0].nodeValue;
    var description = xmlDoc.getElementsByTagName("description")[0].childNodes[0].nodeValue;
    
    appCategories = xmlDoc.getElementsByTagName("category");
   
    
    for (i = 0; i <appCategories.length; i++) {
        var catDetails = appCategories[i].children; //outputs array of details

        catType = catDetails[0].childNodes[0].nodeValue;
        catTitle = catDetails[1].childNodes[0].nodeValue;
        catDescription = catDetails[2].childNodes[0].nodeValue;
        console.log(description);
        
        var formInput = '<div class="col-md-4 "><div class=" panel panel-default"><div class="panel-body"><h2>'+catTitle +'</h2><p>	'+catDescription +'</p><br> <button type="submit" class="btn btn-block btn-primary" name="category" value="'+catType +'">Select	</button></div>	</div></div>';    
    }   
}
*/
</script>
	

  </body>
</html>