<%@ page import="java.io.*,java.util.*, javax.servlet.*, java.nio.*, java.net.*" %>
<%@ page import="javax.servlet.http.*" %>
<%@ page import="org.apache.commons.fileupload.*" %>
<%@ page import="org.apache.commons.fileupload.disk.*" %>
<%@ page import="org.apache.commons.fileupload.servlet.*" %>


<%
// page import="org.apache.commons.io.output.*" //seems to cause a bug
   File file ;
	String host = request.getParameter("host");
	String inputType = request.getParameter("inputType");
	int port = Integer.parseInt(request.getParameter("port"));
	//System.out.println(inputType);
	//System.out.println(host);

   int maxFileSize = 5000 * 1024;
   int maxMemSize = 5000 * 1024;
   ServletContext context = pageContext.getServletContext();
   String filePath = context.getInitParameter("file-upload");

   // Verify the content type
   String contentType = request.getContentType();
   if ((contentType.indexOf("multipart/form-data") >= 0)) {

      DiskFileItemFactory factory = new DiskFileItemFactory();
      // maximum size that will be stored in memory
      factory.setSizeThreshold(maxMemSize);
      // Location to save data that is larger than maxMemSize.
      factory.setRepository(new File("c:\\temp"));

      // Create a new file upload handler
      ServletFileUpload upload = new ServletFileUpload(factory);
      // maximum file size to be uploaded.
      upload.setSizeMax( maxFileSize );
      try{ 
         // Parse the request to get file items.
         List fileItems = upload.parseRequest(request);

         // Process the uploaded file items
         Iterator i = fileItems.iterator();

         //out.println("<html>");
         //out.println("<head>");
        // out.println("<title>JSP File upload</title>");  
         //out.println("</head>");
        // out.println("<body>");
         while ( i.hasNext () ) 
         {
            FileItem fi = (FileItem)i.next();
            
            //String ifName;
            //String ifValue;
            
           // if ( fi.isFormField () ){
            	//If it's a form field, requet.getParameter() doesn't work with enctype=multipart/...
            	
            	  // ifName = fi.getFieldName();//text1
                   //ifValue = fi.getString();
                   //System.out.println(ifName+" : "+ ifValue);
                   //Put them in an array and use later.
            	 
           // }
            
            if ( !fi.isFormField () )	
            {	//If it's not a form field, it's a file. Treat it
				// Get the uploaded file parameters
				
				String fieldName = fi.getFieldName();
				String fileName = fi.getName();
				boolean isInMemory = fi.isInMemory();
				long sizeInBytes = fi.getSize();
				
				// Write the file
				if( fileName.lastIndexOf("\\") >= 0 ){
					file = new File( filePath + 
					fileName.substring( fileName.lastIndexOf("\\"))) ;
				}else{
					file = new File( filePath + 
					fileName.substring(fileName.lastIndexOf("\\")+1)) ;
				}
				fi.write( file ) ;
				//out.println("Uploaded Filename: " + filePath + fileName + "<br>");
				
				
				
				try {
					Socket serviceSocket = null;
					DataOutputStream outputStream = null;
					DataInputStream inputStream = null;
		
					//initialize input and output streams
					//serviceSocket = new Socket("163.143.88.162", 10000);
					serviceSocket = new Socket(host, port);
					outputStream = new DataOutputStream(serviceSocket.getOutputStream());
					inputStream = new DataInputStream(serviceSocket.getInputStream());
		
					//if all is ok, write data to socket
					if(serviceSocket != null && outputStream != null && inputStream != null){
						try{
							try{
								
								//File myFile = new File("client.txt");
								File myFile = file;
								BufferedInputStream in = new BufferedInputStream(new FileInputStream(myFile));
								int size = in.available();
								int count;
								
								//convert to int. default if long
								int inputFileSize = (int) myFile.length();
								byte[] data = new byte[(int) myFile.length()];
								
								
								
								
								
								
								
								
								
								//Send INIT command
								//header	
									//int inputValue = 0;
							
									System.out.println(inputType);
									
									byte[] command1 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(0).array();
									byte[] messageNumber1 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(1).array();
									byte[] numberOfMessages1 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(1).array();
									byte[] dataType1 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(0).array();
									byte[] dataLengthInByteArray1 = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(0).array();
							
							
									ByteArrayOutputStream os1 = new ByteArrayOutputStream();
									os1.write(command1);
									os1.write(messageNumber1);
									os1.write(numberOfMessages1);
									os1.write(dataType1);
									os1.write(dataLengthInByteArray1);
							
									//create a byte array with all the header content put together
									byte[] header1 = os1.toByteArray();
									
									//write the header, then the text, then flush/send
									outputStream.write(header1);						
									while ((count = in.read(data)) > 0) {
										 outputStream.write(data, 0, count);
									}
									outputStream.flush();
									
									//server response
									//Create 20 digit array for response header, plus lenght of inputFile
									int responseSize1 = 20;
									byte[] responseHeader1 = new byte[responseSize1];
									int j1=0;

									while(j1 < responseSize1){
										//System.out.println(inputStream.read());
										responseHeader1[j1]=inputStream.readByte();
										j1++;
									}
									
									//convert responseHeader from byte array to integer array
									IntBuffer intBuf1 = ByteBuffer.wrap(responseHeader1).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer();
									int[] responseIntArray1 = new int[intBuf1.remaining()];
									intBuf1.get(responseIntArray1);
									
									
									//create a buffer to hold response data
									//StringBuffer responseData1 = new StringBuffer();
									//String rd1;
									//while((rd1=inputStream.readLine())!= null){
									//	responseData1.append(rd1);
									//}

									//System.out.println(responseData);								
									///System.out.println("Data size:" + size);
									//System.out.println("Data length:" + inputFileSize);
									System.out.println("Header sent:" + Arrays.toString(header1));
									//System.out.println("Data sent:" + inputText);
									//System.out.println("Data sent(bytes):" + Arrays.toString(data1));
									System.out.println("Server response header: " + Arrays.toString(responseHeader1));
							 		//System.out.println("Server response data: " + responseData);
							 		//System.out.println("Input type: " + inputType);
							 		//Display this result in webpage
							 		//out.println("Result: " + responseData1);
															
									
		
		
		
		
		
		
		
							
								
							}
							 catch (Exception ex) {
								System.out.println("File not found. ");
							}
							
							inputStream.close();
							outputStream.close();
							serviceSocket.close();
							
						}catch (IOException e) {
							System.out.println("Connection to server failed with error:" + e);
							//e.printStackTrace();
						}
					}
		
				}
				catch (IOException e) {
				System.out.println("Connection to server failed :" + e);
				out.println("Connection to server failed. Please try again later");
				//e.printStackTrace();
			}
			
				
				
				
		
				
				
				
				
				
				
				
				
				
				
            }
            
           
         }
		
		
         out.println("</body>");
         out.println("</html>");
      }catch(Exception ex) {
         System.out.println("Other error: " + ex);
         //System.exit(1);
      } 
//Socket connection
			
		 	  
   }else{
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet upload</title>");  
      out.println("</head>");
      out.println("<body>");
      out.println("<p>No file uploaded</p>"); 
      out.println("</body>");
      out.println("</html>");
   }
   
   

%>