<!DOCTYPE html>
<%@ page language="java" contentType="text/html" %>
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bootstrap 3, from LayoutIt!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/dropzone.css" rel="stylesheet">

  </head>
  <body>
   <div id="loading" style="display:none;">
	  <img id="loading-image" src="images/ajax-loader.gif" alt="Loading..." />
	</div>
	  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="${pageContext.request.contextPath}/index.jsp">University of Aizu</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1><span style="text-transform: capitalize">${requestScope.category}</span> processing applications</h1>
        <p>Providing Deep Learning as a Cloud Service</p>
        <!--<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>-->
      </div>
    </div>
	
	
	
	
	
	
    <div class="container-fluid">
    <ol class="breadcrumb">
	  <li><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
	  <li class="active"><span style="text-transform: capitalize">${requestScope.category}</span> application directory</li>
	</ol>
		
	<div class="row">
		
			<!--<div class="page-header">
				<h1>
					DL Cloud <small>dd</small>
				</h1>
			</div>-->
			
			<form action="StartService" method="POST" id="start-service-form">
				<div class="row">
				    <c:forEach items="${appList}" var="app">
					    <div class="col-md-3 ">
							<div class="col-md-12 panel panel-default">
								<div class="panel-body">
									
										<c:forEach items="${app.value}" var="item" varStatus="loop">
									       <h2>${item.key} </h2>
									        <p>${item.value}</p>
									    </c:forEach>
									
									<div class="row">
										<div class="col-md-12">
											
												
												<button type="submit" class="btn btn-block btn-primary select-srvc-btn" name="serviceSelected" value="${app.key}">
													Select
												</button>
											
										</div>
									</div>
								</div>
							</div>
						</div>	      
				    </c:forEach>
				
				</div>
				</form>	
				
			<div class="row">
				<div class="col-md-12">
					University of Aizu, 2016.
				</div>
			</div>
			
		
	</div>
</div>




    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>

	<script type="text/javascript">
		//when an app is clicked, show the loading buttong
		$(".select-srvc-btn").click(function(){
			// $('#loading').show();
		});
		
		
		//load in a new window
		var myForm = document.getElementById('start-service-form');
		myForm.onsubmit = function() {
		    var w = window.open('about:blank','Popup_Window','width=1366,height=768');
		    w.document.write('<center><h3 style="margin-top:25%;font-family: Segoe UI,Arial, sans-serif;">Initializing service</h3><img id="loading-image" src="images/loading.gif" alt="..." /></center>');
		    this.target = 'Popup_Window';
		};
		
	
	</script>

  </body>
</html>