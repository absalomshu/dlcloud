<!DOCTYPE html>
<%@ page language="java" contentType="text/html" %>
<%@ page import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bootstrap 3, from LayoutIt!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/dropzone.css" rel="stylesheet">

  </head>
  <body>
   <div id="loading" style="display:none;">
	  <img id="loading-image" src="images/ajax-loader.gif" alt="Loading..." />
	</div>
	  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">University of Aizu</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Manage applications</h1>
        <p>Providing Deep Learning as a Cloud Service</p>
        <!--<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>-->
      </div>
    </div>
	
    <div class="container-fluid">
	<div class="row">
		
			
			<form action="AddService" method="POST">
		
				    <div class="form-group">
				        <label for="inputEmail" class="control-label col-xs-2">Name</label>
				        <div class="col-xs-10">
				            <input type="email" class="form-control" id="inputEmail" placeholder="e.g. image coloring">
				        </div>
				    </div>
				    <div class="form-group">
				        <label for="inputPassword" class="control-label col-xs-2">Description</label>
				        <div class="col-xs-10">
				            <input type="text" class="form-control" id="inputPassword" placeholder="e.g. automatically colour black on white images">
				        </div>
				    </div>
				    <div class="form-group">
				        <label for="inputPassword" class="control-label col-xs-2">Input type</label>
				        <div class="col-xs-10">
				            <select  class="form-control" >
							  <option value="image">Image</option>
							  <option value="text">Text</option>
							  <option value="audio">Audio</option>
							</select>
				        </div>
				    </div>
				    <div class="form-group">
				        <label for="inputPassword" class="control-label col-xs-2">Output type</label>
				        <div class="col-xs-10">
				            <select  class="form-control" >
							  <option value="image">Image</option>
							  <option value="text">Text</option>
							  <option value="audio">Audio</option>
							</select>
				        </div>
				    </div>
				    <div class="form-group">
				        <label for="inputPassword" class="control-label col-xs-2">Host IP</label>
				        <div class="col-xs-10">
				            <input type="text" class="form-control" id="inputPassword" placeholder="e.g. 123.123.1.123">
				        </div>
				    </div>
				    <div class="form-group">
				        <label for="inputPassword" class="control-label col-xs-2">Host user</label>
				        <div class="col-xs-10">
				            <input type="text" class="form-control" id="inputPassword" placeholder="e.g. admin">
				        </div>
				    </div>
				    <div class="form-group">
				        <label for="inputPassword" class="control-label col-xs-2">Host password</label>
				        <div class="col-xs-10">
				            <input type="text" class="form-control" id="inputPassword" placeholder="e.g. password">
				        </div>
				    </div>
				    <div class="form-group">
				        <label for="inputPassword" class="control-label col-xs-2">Service port</label>
				        <div class="col-xs-10">
				            <input type="text" class="form-control" id="inputPassword" placeholder="e.g. 10000">
				        </div>
				    </div>
				    <div class="form-group">
				        <label for="inputPassword" class="control-label col-xs-2">Service start command</label>
				        <div class="col-xs-10">
				            <input type="text" class="form-control" id="inputPassword" placeholder="e.g. nvidia-docker run -i --rm -p 10010:10000 color_service">
				        </div>
				    </div>
				  
				    <div class="form-group">
				        <div class="col-xs-offset-2 col-xs-10">
				           &nbsp;
				        </div>
				    </div>
				    <div class="form-group">
				        <div class="col-xs-offset-2 col-xs-10">
				            <button type="submit" class="btn btn-primary">Save</button>
				            <a href="${pageContext.request.contextPath}/index.jsp" class="btn btn-default">	Cancel	</a>
				        </div>
				    </div>
				</form>
				
			
			
				
			<div class="row">
				<div class="col-md-12">
					University of Aizu, 2016.
				</div>
			</div>
			
		
	</div>
</div>




    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>

	<script type="text/javascript">
		//when an app is clicked, show the loading buttong
		$(".select-srvc-btn").click(function(){
			 $('#loading').show();
		});
		
	
	</script>

  </body>
</html>