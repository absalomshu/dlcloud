<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bootstrap 3, from LayoutIt!</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/dropzone.css" rel="stylesheet">

  </head>
  <body>
	  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
           <a class="navbar-brand" href="${pageContext.request.contextPath}/index.jsp">University of Aizu</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Deep Learning Cloud</h1>
        <p>Providing Deep Learning as a Cloud Service</p>
        <!--<p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>-->
      </div>
    </div>
	
    <div class="container-fluid">
    <%
    
    //Display notice if there is.
    if(null != session.getAttribute("notice")){
    	out.println("<div class='alert alert-warning'><a href='#' class='close' data-dismiss='alert'>&times;</a>");  
    	out.println(session.getAttribute("notice"));
    	out.println("</div>");
    	session.removeAttribute("notice");
    }
%>
    
	<div class="row">
		<div class="col-md-12">
			<ol class="breadcrumb">
			  
			  <li class="active">Home</li>
			</ol>
			
			<div class="row">
				
				
				<form action="directory" method="GET" >
		        
				<div class="col-md-4 ">
					<div class=" panel panel-default">
						<div class="panel-body">
							<h2>
								Text processing services
							</h2>
							<p>
								Text input with various output
								
							</p><br>
							 	<!-- <input type="hidden"  name="" value="demo_service">-->
								<button type="submit" class="btn btn-block btn-primary" name="category" value="text">Select	</button>
							
						
						</div>
					</div>
				</div>
				
				<div class="col-md-4 ">
					<div class=" panel panel-default">
						<div class="panel-body">
							<h2>
								Image processing services
							</h2>
							<p>
								Image input with various output
								
							</p><br>
							 	<!--<input type="hidden"  name="serviceSelected" value="image_composition">-->
								<button type="submit" class="btn btn-block btn-primary" name="category" value="image">Select	</button>
							
						
						</div>
					</div>
				</div>
				
				
				<div class="col-md-4 ">
					<div class=" panel panel-default">
						<div class="panel-body">
							<h2>
								Audio processing services
							</h2>
							<p>
								Audio input with various output
								
							</p><br>
								<button type="submit" class="btn btn-block btn-primary" name="category" value="audio">Select	</button>
							
						
						</div>
					</div>
				</div>
				
				</form>
				
				
				<form action="manage" method="GET" >
					<div class="col-md-4 ">
					<button type="submit" class="btn " >Manage services	</button>
					</div>
				</form>
			</div>
			
		
			<div class="row">
				<div class="col-md-12">
					University of Aizu, 2016.
				</div>
			</div>
			
		</div>
	</div>
</div>




    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>

	

  </body>
</html>