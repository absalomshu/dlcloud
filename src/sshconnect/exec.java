/* -*-mode:java; c-basic-offset:2; indent-tabs-mode:nil -*- */
/**
 * This program will demonstrate remote exec.
 *   $ CLASSPATH=.:../build javac Exec.java 
 *   $ CLASSPATH=.:../build java Exec
 * You will be asked username, hostname, displayname, passwd and command.
 * If everything works fine, given command will be invoked 
 * on the remote side and outputs will be printed out.
 *
 */
package sshconnect;
import com.jcraft.jsch.*;
import java.awt.*;
import javax.swing.*;
import java.io.*;

public class exec{
	
	private String host = null;
	private String user = null;
	private String password = null;
	private String command = null;
	
	public exec(String host, String user, String password, String command){

		this.host = host;
		this.user = user;
		this.password = password;
		this.command = command;
	}
	
	
	
	public static void main(String[] arg){
		
	}
  
	public void SshConnect(){
	    try{
	      JSch jsch=new JSch();  
		  /*
	      String host=null;
	      if(arg.length>0){
	        host=arg[0];
	      }
	      else{
	        host=JOptionPane.showInputDialog("Enter username@hostname",
	                                         System.getProperty("user.name")+
	                                         "@localhost"); 
	      }
	      String user=host.substring(0, host.indexOf('@'));
	      host=host.substring(host.indexOf('@')+1); */
	
		 // String host="dl-srv1.u-aizu.ac.jp";
	     // String user="service";
	      
	      System.out.println("Exec.java: User: "+user +" Host: "+ host+" Command: "+ command);
	      Session session=jsch.getSession(user, host, 22);
	      
	      /*
	      String xhost="127.0.0.1";
	      int xport=0;
	      String display=JOptionPane.showInputDialog("Enter display name", 
	                                                 xhost+":"+xport);
	      xhost=display.substring(0, display.indexOf(':'));
	      xport=Integer.parseInt(display.substring(display.indexOf(':')+1));
	      session.setX11Host(xhost);
	      session.setX11Port(xport+6000);
	      */
	
	      // username and password will be given via UserInfo interface.
	      UserInfo ui=new MyUserInfo();
	      session.setUserInfo(ui);
	      session.connect();
			
		//Fix command, and prevent dialog prompt
			//String command = "cd echoService/ ; python3 echoService.py";
			//String command = "nvidia-docker run -i --rm -p 10010:10000 deepdream_service";
			
	      //String command=JOptionPane.showInputDialog("Enter command", "set|grep SSH");
	
	      Channel channel=session.openChannel("exec");
	      ((ChannelExec)channel).setCommand(command);
	      
	      
	      // X Forwarding
	      // channel.setXForwarding(true);
	
	      //channel.setInputStream(System.in);
	      channel.setInputStream(null);
	
	      //channel.setOutputStream(System.out);
	
	      //FileOutputStream fos=new FileOutputStream("/tmp/stderr");
	      //((ChannelExec)channel).setErrStream(fos);
	      ((ChannelExec)channel).setErrStream(System.err);
	
	      InputStream in=channel.getInputStream();
	
	      channel.connect();
	
	      byte[] tmp=new byte[1024];
	      while(true){
	        while(in.available()>0){
	          int i=in.read(tmp, 0, 1024);
	         // if(i<0)break;

	          System.out.print(new String(tmp, 0, i));
	          //This break is forced. If not, you need to click a service twice to start it for some reason. The trade off is you don't read response from server
	          //THe first time you start the service, it gets stuck in this loop. in.read reads forever. Then when you click again, it goes through.
	          //antoher break is inserted for same purpose a few lines further
	          break;
	          
	        }
	        if(channel.isClosed()){
	          if(in.available()>0) continue; 
	          System.out.println("exit-status: "+channel.getExitStatus());
	          break;
	        }
	        try{Thread.sleep(1000);}catch(Exception ee){}
	        //2nd forced break
	        break;
	      }
	      channel.disconnect();
	      session.disconnect();
	    }
	    catch(Exception e){
	      System.out.println(e);
	    }
  }

  public static class MyUserInfo implements UserInfo, UIKeyboardInteractive{
    public String getPassword(){ return passwd; }
    public boolean promptYesNo(String str){
      //Force confirmation of initial server message about credentials
	  /*Object[] options={ "yes", "no" };
      int foo=JOptionPane.showOptionDialog(null, 
             str,
             "Warning", 
             JOptionPane.DEFAULT_OPTION, 
             JOptionPane.WARNING_MESSAGE,
             null, options, options[0]);
       return foo==0;*/
		return true;
    }
  
    String passwd;
    //JTextField passwordField=(JTextField)new JPasswordField(20);

    public String getPassphrase(){ return null; }
    public boolean promptPassphrase(String message){ return true; }
    public boolean promptPassword(String message){
     
	/*	
	  Object[] ob={passwordField}; 
      int result=
        JOptionPane.showConfirmDialog(null, ob, message,
                                      JOptionPane.OK_CANCEL_OPTION);
      if(result==JOptionPane.OK_OPTION){
        passwd=passwordField.getText();
        return true;
      }
      else{ 
        return false; 
      } */
		//Cancel prompt and manually send password
		passwd = "service";
		return true;
    }
    public void showMessage(String message){
      JOptionPane.showMessageDialog(null, message);
    }
    final GridBagConstraints gbc = 
      new GridBagConstraints(0,0,1,1,1,1,
                             GridBagConstraints.NORTHWEST,
                             GridBagConstraints.NONE,
                             new Insets(0,0,0,0),0,0);
    private Container panel;
    public String[] promptKeyboardInteractive(String destination,
                                              String name,
                                              String instruction,
                                              String[] prompt,
                                              boolean[] echo){
      panel = new JPanel();
      panel.setLayout(new GridBagLayout());

      gbc.weightx = 1.0;
      gbc.gridwidth = GridBagConstraints.REMAINDER;
      gbc.gridx = 0;
      panel.add(new JLabel(instruction), gbc);
      gbc.gridy++;

      gbc.gridwidth = GridBagConstraints.RELATIVE;

      JTextField[] texts=new JTextField[prompt.length];
      for(int i=0; i<prompt.length; i++){
        gbc.fill = GridBagConstraints.NONE;
        gbc.gridx = 0;
        gbc.weightx = 1;
        panel.add(new JLabel(prompt[i]),gbc);

        gbc.gridx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.weighty = 1;
        if(echo[i]){
          texts[i]=new JTextField(20);
        }
        else{
          texts[i]=new JPasswordField(20);
        }
        panel.add(texts[i], gbc);
        gbc.gridy++;
      }

      if(JOptionPane.showConfirmDialog(null, panel, 
                                       destination+": "+name,
                                       JOptionPane.OK_CANCEL_OPTION,
                                       JOptionPane.QUESTION_MESSAGE)
         ==JOptionPane.OK_OPTION){
        String[] response=new String[prompt.length];
        for(int i=0; i<prompt.length; i++){
          response[i]=texts[i].getText();
        }
	return response;
      }
      else{
        return null;  // cancel
      }
    }
  }
}