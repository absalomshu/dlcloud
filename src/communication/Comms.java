package communication;

import java.net.*;
import java.io.*;
import java.nio.*;
import java.util.Arrays;
import java.awt.*;
import javax.imageio.*;
import java.awt.image.BufferedImage;

public class Comms {
	
	protected static Socket serviceSocket = null;
	protected static DataOutputStream outputStream = null;
	protected static DataInputStream inputStream = null;
	
	
	public static void main(String[] args){
		ConnectService("dl-srv1.u-aizu.ac.jp", 10010);
		//ConnectService("163.143.88.162", 10000);
		//InitService();
		SendFileRequest();
	}
	
	
	public static void ConnectService(String host, int port){
		try {
			//initialize input and output streams
			//serviceSocket = new Socket("163.143.88.162", 10000);
			serviceSocket = new Socket(host, port);
			outputStream = new DataOutputStream(serviceSocket.getOutputStream());
			inputStream = new DataInputStream(serviceSocket.getInputStream());
		}catch (IOException e) {
			System.out.println("Connection to server failed :" + e);
			//e.printStackTrace();
		}
	}
	
	public static void InitService(){
		
		//if all is ok, write data to socket
		if(serviceSocket != null && outputStream != null && inputStream != null){
			try{
				try{
					//create header	
					byte[] header = SendHeader(0,1,1,0,0);

					//get server response
					int[] responseHeader = GetResponseHeader();
					StringBuffer responseBody = GetResponseBody();


					//System.out.println(responseData);								
					//System.out.println("Data size:" + size);
					//System.out.println("Data length:" + inputFileSize);
					System.out.println("Header sent:" + Arrays.toString(header));
					//System.out.println("Data sent:" + inputText);
					//System.out.println("Data sent(bytes):" + Arrays.toString(data));
					System.out.println("Server response header: " + Arrays.toString(responseHeader));
					///System.out.println("Server response data: " + responseData);
					//System.out.println("Input type: " + inputType);
					//Display this result in webpage
					//System.out.println("Result: " + responseData);

				}
				 catch (Exception ex) {
					System.out.println("File not found. ");
				}

				inputStream.close();
				outputStream.close();
				serviceSocket.close();

			}catch (IOException e) {
				System.out.println("Connection to server failed with error:" + e);
				//e.printStackTrace();
			}
		}

		
	}
		
	public static byte[] SendHeader(int cmd, int msgNbr, int nbrOfMsgs, int dtType, int dtLgth){
		
		byte[] header = new byte[5];
		try{
			byte[] command = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(cmd).array();
			byte[] messageNumber = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(msgNbr).array();
			byte[] numberOfMessages = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(nbrOfMsgs).array();
			byte[] dataType = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(dtType).array();
			byte[] dataLengthInByteArray = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(dtLgth).array();

			ByteArrayOutputStream os = new ByteArrayOutputStream();
			os.write(command);
			os.write(messageNumber);
			os.write(numberOfMessages);
			os.write(dataType);
			os.write(dataLengthInByteArray);

			header = os.toByteArray();
			//write the header to output
			outputStream.write(header);						
			outputStream.flush();

		}catch (IOException e) {
			System.out.println("Connection to server failed :" + e);
		}
		
		return header;

	}
	
	public static int[] GetResponseHeader(){
		//server response
		//Create 20 digit array for response header, plus lenght of inputFile
		int responseSize = 20;
		byte[] responseHeader = new byte[responseSize];
		int j=0;

		while(j < responseSize){
			try{
				responseHeader[j]=inputStream.readByte();
			j++;
			}catch (IOException e) {
				System.out.println("Error getting response header:" + e);
			}
		}

		//convert responseHeader from byte array to integer array
		IntBuffer intBuf = ByteBuffer.wrap(responseHeader).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer();
		int[] responseIntArray = new int[intBuf.remaining()];
		intBuf.get(responseIntArray);
		
		//return responseHeader;
		return responseIntArray;
	}
	
	
	public static StringBuffer GetResponseBody(){
		//create a buffer to hold response data
		StringBuffer responseData = new StringBuffer();
		String rd;
		try{
			while((rd=inputStream.readLine())!= null){
				responseData.append(rd);
			} 
		}catch (IOException e) {
				System.out.println("Error getting response body:" + e);
		}
		
		return responseData;
	}
	
	
	public static void SendTextRequest(){
		
		//if all is ok, write data to socket
		if(serviceSocket != null && outputStream != null && inputStream != null){
			try{
				try{
					//data
					String inputText = "Java in many ways is a very eccentric programming language.  to pre-allocate the whole array and copy the bytes into it.";
				
					//store text as bytes in data byte array
					//for some reason, 2 bytes are lost, so I manually add so the complete text is sent
					byte[] data = inputText.getBytes("utf-8");	
					int dataLength = 2 + data.length;
					
					//create header	
					byte[] header = SendHeader(1,1,1,1,dataLength);

					//write the text to output
					outputStream.writeUTF(inputText);
					outputStream.flush();

					//get server response
					int[] responseHeader = GetResponseHeader();
					StringBuffer responseBody = GetResponseBody();


					System.out.println("Header sent:" + Arrays.toString(header));
					System.out.println("Server response header: " + Arrays.toString(responseHeader));
					System.out.println("Result: " + responseBody);

				}
				 catch (Exception ex) {
					System.out.println("SendTextRequest error "+ ex);
				}

				inputStream.close();
				outputStream.close();
				serviceSocket.close();

			}catch (IOException e) {
				System.out.println("Connection to server failed with error:" + e);
				//e.printStackTrace();
			}
		}

		
	}
	
	
		public static void SendFileRequest(){
		
			  
		//if all is ok, write data to socket
		if(serviceSocket != null && outputStream != null && inputStream != null){
			try{
				try{
					
					//OUTPUT STREAM
					BufferedImage image = ImageIO.read(new File("dance.jpg"));
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					ImageIO.write(image, "jpg", byteArrayOutputStream);

					//byte[] imageArray = ByteBuffer.allocate(4).putInt(byteArrayOutputStream.size()).array();
					int imageSize = (int) byteArrayOutputStream.size();

					byte data[] = byteArrayOutputStream.toByteArray();
					//System.out.println("Data sent:"+Arrays.toString(data));
					
					//create and send header	
					byte[] header = SendHeader(1,1,1,2,imageSize);

					//send body
					outputStream.write(data);
					outputStream.flush();
					
					
					//INPUT STREAM
					int messageNumber;
					int numberOfMessages; 
					int[] responseHeader;
					int responseSize;
					byte[] response;
					int i=0, j=0, defaultCounter = 1;
					
					//Loop and get the response headers and body
					for(int k=1; k<=defaultCounter; k++){
						
						responseHeader = GetResponseHeader();
						messageNumber = responseHeader[1];
						numberOfMessages = responseHeader[2];
						System.out.println("Response header:"+ k + Arrays.toString(responseHeader));

						//Very important! Not resetting i to 0 cost me hours!
						
						i=0;
						//read input into byte array
						response = new byte[responseHeader[4]];
						while(i<responseHeader[4]){
							response[i]=inputStream.readByte();
							i++;
						} 
						
						//write byte array to image file
						InputStream in = new ByteArrayInputStream(response);
						BufferedImage t = ImageIO.read(in);
						ImageIO.write(t,"jpg",new File("works.jpg"));
						
						//Number of runs is only determined after first run
						defaultCounter = numberOfMessages;
					}
					
					
					
					
					
					
					/*
					//NORMAL FILE RECEIVE
					StringBuffer responseData = new StringBuffer();
					String rd;
					try{
						while((rd=inputStream.readLine())!= null){
							responseData.append(rd);
						} 
					}catch (IOException e) {
							System.out.println("Error getting response body:" + e);
					}
					*/
					
					
					
					
				
					
					//try{
						//out = new BufferedOutputStream(new FileOutputStream("im.jpg"));
						//convert the responseData StringBuffer to a byte array
						//out.write(String.valueOf(responseData).getBytes());
						//out.write(image);
						
						//ImageIO.write(image,"jpg",new File("imm.jpg"));
					//}
					//finally{
					//	if(out != null) out.close();
					//}
					
					
					

					//System.out.println("Header sent:" + Arrays.toString(header));
					
					//System.out.println("Result: " + responseData);

				}
				 catch (Exception ex) {
					System.out.println("SendTextRequest error "+ ex);
				}

				inputStream.close();
				outputStream.close();
				serviceSocket.close();

			}catch (IOException e) {
				System.out.println("Connection to server failed with error:" + e);
				//e.printStackTrace();
			}
		}

		
	}
	
	
	
	
	
}
