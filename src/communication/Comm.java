package communication;

import java.net.*;
import java.io.*;
import java.nio.*;
import java.util.Arrays;
import java.util.*;
import java.awt.*;
import javax.imageio.*;
import java.awt.image.BufferedImage;

public class Comm {
	
	 private Socket serviceSocket = null;
	 private DataOutputStream outputStream = null;
	 private DataInputStream inputStream = null;
	 private String host= null;
	 private int port= 0;
	
	//Constructor
	public Comm(String host, int port){

		this.host = host;
		this.port = port;
	}
	
	
	
	public static void main(String[] args){
		//ConnectService("dl-srv1.u-aizu.ac.jp", 10010);
		//ConnectService("163.143.88.162", 10000);
		//InitService();
		//SendFileRequest();
	}
	
	
	public boolean ConnectService(){
		
		boolean connectionSuccessful = false;
		try {
			//initialize input and output streams
			//serviceSocket = new Socket("163.143.88.162", 10000);
			System.out.println("ConnectService: Host:" + host+" Port: " + port+" (Comm.java/ConnectService)");
			serviceSocket = new Socket(host, port);
			outputStream = new DataOutputStream(serviceSocket.getOutputStream());
			inputStream = new DataInputStream(serviceSocket.getInputStream());
			connectionSuccessful = true;
		}catch (IOException e) {
			System.out.println("ConnectService: Connection to server failed :" + e);
			connectionSuccessful = false;
			//e.printStackTrace();
		}
		System.out.println("ConnectService: Connection successful :" + connectionSuccessful);
		return connectionSuccessful;
	}
	
	public  void InitService(){
		
		//if all is ok, write data to socket
		if(serviceSocket != null && outputStream != null && inputStream != null){
			try{
				try{
					//create header	
					byte[] header = SendHeader(0,1,1,0,0);
					
					//get server response
					int[] responseHeader = GetResponseHeader();
					StringBuffer responseBody = GetResponseBody();
					
					
					//System.out.println(responseData);								
					//System.out.println("Data size:" + size);
					//System.out.println("Data length:" + inputFileSize);
					System.out.println("Init header sent:" + Arrays.toString(header));
					//System.out.println("Data sent:" + inputText);
					//System.out.println("Data sent(bytes):" + Arrays.toString(data));
					System.out.println("Server response header: " + Arrays.toString(responseHeader));
					///System.out.println("Server response data: " + responseData);
					//System.out.println("Input type: " + inputType);
					//Display this result in webpage
					//System.out.println("Result: " + responseData);
					
				}
				catch (Exception ex) {
					System.out.println("Init service error. ");
				}
				
				inputStream.close();
				outputStream.close();
				serviceSocket.close();
				
			}catch (IOException e) {
				System.out.println("Connection to server failed with error:" + e);
				//e.printStackTrace();
			}
		}
		
		
	}
	public  void InitServiceWithParameters(String intermediateFile){
		
		//if all is ok, write data to socket
		if(serviceSocket != null && outputStream != null && inputStream != null){
			try{
				try{
					
					System.out.println("Intermediate file"+intermediateFile);
					
					/* WORKS, BUT GIVES SLIGHTLY DIFFERENT IMAGE SIZE
					BufferedImage image = ImageIO.read(new File(intermediateFile));
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					ImageIO.write(image, "jpg", byteArrayOutputStream);				
					int imageSize = (int) byteArrayOutputStream.size();					
					byte intData[] = byteArrayOutputStream.toByteArray();
					*/
					
					//This method returns exact file size as it is. Using BufferedImage above returns an image slightly larger
					
					FileInputStream fis = new FileInputStream(intermediateFile);

					ByteArrayOutputStream bos = new ByteArrayOutputStream();
					byte[] buf = new byte[1024];
					try {
					    for (int readNum; (readNum = fis.read(buf)) != -1;) {
					        bos.write(buf, 0, readNum); 
					    }
					} catch (Exception ex) {

					}
					byte[] data = bos.toByteArray();
					int imageSize = (int) bos.size();	
					
					//System.out.println("IMAGE SIZE ii"+bos.size());
					
					System.out.println("SENDING INIT COMMAND");
					System.out.println("IMAGESIZE"+imageSize);
					
					
					//create and send header	
					byte[] header = SendHeader(0,1,1,2,imageSize);
					//System.out.println("File request header sent:" + Arrays.toString(header));
										
					
					//send body
					outputStream.write(data);
					outputStream.flush();
					
					System.out.println("WRITING DATA TO OUTPUT");
					
					System.out.println("Data sent(bytes):" + Arrays.toString(data));
					//GetResponse();
					
					
					
					//-------------------------------------------------------------------
					
					//create header	
					//byte[] header = SendHeader(0,1,1,0,0);

					//get server response
					int[] responseHeader = GetResponseHeader();
					StringBuffer responseBody = GetResponseBody();

					System.out.println("Init header sent:" + Arrays.toString(header));
					System.out.println("Server response header: " + Arrays.toString(responseHeader));			
				}
				 catch (Exception ex) {
					System.out.println("Init service error. "+ex);
				}

				inputStream.close();
				outputStream.close();
				serviceSocket.close();

			}catch (IOException e) {
				System.out.println("Connection to server failed with error:" + e);

			}
		}

		
	}
		
	public  byte[] SendHeader(int cmd, int msgNbr, int nbrOfMsgs, int dtType, int dtLgth){
		
		byte[] header = new byte[5];
		try{
			byte[] command = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(cmd).array();
			byte[] messageNumber = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(msgNbr).array();
			byte[] numberOfMessages = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(nbrOfMsgs).array();
			byte[] dataType = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(dtType).array();
			byte[] dataLengthInByteArray = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(dtLgth).array();

			ByteArrayOutputStream os = new ByteArrayOutputStream();
			os.write(command);
			os.write(messageNumber);
			os.write(numberOfMessages);
			os.write(dataType);
			os.write(dataLengthInByteArray);

			header = os.toByteArray();
			//write the header to output
			outputStream.write(header);						
			outputStream.flush();

		}catch (IOException e) {
			System.out.println("Connection to server failed :" + e);
		}
		
		return header;

	}
	
	public  int[] GetResponseHeader(){
		//server response
		//Create 20 digit array for response header, plus lenght of inputFile
		int responseSize = 20;
		byte[] responseHeader = new byte[responseSize];
		int j=0;

		while(j < responseSize){
			try{
				responseHeader[j]=inputStream.readByte();
			j++;
			}catch (IOException e) {
				System.out.println("Error getting response header:" + e);
				//j++;continue;
			}
		}

		//convert responseHeader from byte array to integer array
		IntBuffer intBuf = ByteBuffer.wrap(responseHeader).order(ByteOrder.LITTLE_ENDIAN).asIntBuffer();
		int[] responseIntArray = new int[intBuf.remaining()];
		intBuf.get(responseIntArray);
		
		//return responseHeader;
		return responseIntArray;
	}
	
	
	public  StringBuffer GetResponseBody(){
		//create a buffer to hold response data
		StringBuffer responseData = new StringBuffer();
		String rd;
		try{
			while((rd=inputStream.readLine())!= null){
				responseData.append(rd);
			} 
		}catch (IOException e) {
				System.out.println("Error getting response body:" + e);
		}
		System.out.println("GetResponseBody Response Data: " + responseData);
		return responseData;
	}
	
	
	public StringBuffer  SendTextRequest(String inputText){
		
		StringBuffer responseBody = null;
		
		//if all is ok, write data to socket
		if(serviceSocket != null && outputStream != null && inputStream != null){
			try{
				try{
					//data
					//String inputText = "Java in many ways is a very eccentric programming language.  to pre-allocate the whole array and copy the bytes into it.";
				
					//store text as bytes in data byte array
					//for some reason, 2 bytes are lost, so I manually add so the complete text is sent
					byte[] data = inputText.getBytes("utf-8");	
					int dataLength = 2 + data.length;
					
					//create header	
					byte[] header = SendHeader(1,1,1,1,dataLength);

					//write the text to output
					outputStream.writeUTF(inputText);
					outputStream.flush();

					//get server response
					int[] responseHeader = GetResponseHeader();
					responseBody = GetResponseBody();


					System.out.println("Header sent:" + Arrays.toString(header));
					System.out.println("Server response header: " + Arrays.toString(responseHeader));
					System.out.println("Result: " + responseBody);
					
					//return responseBody;
				}
				 catch (Exception ex) {
					System.out.println("SendTextRequest error "+ ex);
				}

				inputStream.close();
				outputStream.close();
				serviceSocket.close();

			}catch (IOException e) {
				System.out.println("Connection to server failed with error:" + e);
				//e.printStackTrace();
			}
		}

		return responseBody;
	}
	
	
	public  ArrayList SendImageRequest(String theFile){
		
		ArrayList fullResponse = new ArrayList();
		//if all is ok, write data to socket
		if(serviceSocket != null && outputStream != null && inputStream != null){
			try{
				try{
					
					//OUTPUT STREAM
					//BufferedImage image = ImageIO.read(new File("dance.jpg"));
					BufferedImage image = ImageIO.read(new File(theFile));
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					ImageIO.write(image, "jpg", byteArrayOutputStream);
					
					//byte[] imageArray = ByteBuffer.allocate(4).putInt(byteArrayOutputStream.size()).array();
					int imageSize = (int) byteArrayOutputStream.size();
					
					byte data[] = byteArrayOutputStream.toByteArray();
					//System.out.println("Data sent:"+Arrays.toString(data));
					
					//create and send header	
					byte[] header = SendHeader(1,1,1,2,imageSize);
					System.out.println("File request header sent:" + Arrays.toString(header));
					
					
					//send body
					outputStream.write(data);
					outputStream.flush();
					
					
					fullResponse = GetResponse();
					
					
				}
				catch (Exception ex) {
					System.out.println("SendImageRequest: Upload error "+ ex);
				}
				
				inputStream.close();
				outputStream.close();
				serviceSocket.close();
				
			}catch (IOException e) {
				System.out.println("SendImageRequest: Failed with error:" + e);
				//e.printStackTrace();
			}
		}
		return fullResponse;
	}
		
	
	
	
	private ArrayList GetResponse(){
		
		ArrayList<Object> fullResponse = new ArrayList<Object>();
		int outputDataFormat = 0;
		
		try{	
			
			//Get full response: Header and body
			//an array of byte arrays
			
			int messageNumber;
			int numberOfMessages; 
			int dataSize; 
			int dataType; 
			int[] responseHeader;
			int responseSize;
			
			int i=0, j=0, defaultCounter = 1;
			
			//Loop and get the response headers and body
			for(int k=1; k<=defaultCounter; k++){
				
				responseHeader = GetResponseHeader();
				messageNumber = responseHeader[1];
				numberOfMessages = responseHeader[2];
				dataType = responseHeader[3];
				dataSize = responseHeader[4];
				System.out.println("Response header:"+ k + Arrays.toString(responseHeader));
				
				
				System.out.println("Response data type:"+dataType);
				
				
				//Very important! Not resetting i to 0 cost me hours!
				i=0;
				
				//read input into byte array of the size specified in 4th param of message header
				byte[] response;
				response = new byte[dataSize];
				while(i<dataSize){
					response[i]=inputStream.readByte();
					i++;
				} 
				
				
				System.out.println("Response body:"+ k + Arrays.toString(response));

				
				//create an array and put all results 
				//start at zero, which is k-1. If not, arrayoutofbounds error
				//0th element is the data type
				
				outputDataFormat = dataType;
				//fullResponse.add(k-1, Arrays.toString(response));
				fullResponse.add(response);
				
				//Ift it's text
				if(dataType ==1){
					
					String responseData = new String(response,"UTF-8");
					System.out.println("GetResponseBody Response Data: " + responseData);
					
					
				}
				//BufferedImage t = ImageIO.read(in);
				//ImageIO.write(t,"jpg",new File("C:/Users/Absalom/Desktop/Java/apache-tomcat-7.0.70/webapps/dlcloud/results/works.jpg"));
				
				//Number of runs is only determined after first run
				defaultCounter = numberOfMessages;
			}
			
			System.out.println("Full response:" + Arrays.toString(fullResponse.toArray()));
			System.out.println("Full response size:"+fullResponse.size());
			
			//Add the response data type as first value of the arraylist
			fullResponse.add(0, outputDataFormat);
			
		}
		catch (Exception ex) {
			System.out.println("SendImageRequest: Upload error "+ ex);
		}
		
		return fullResponse;
	}
	
	
	
	
	
	
	
	
	
	
	public  void SendFileRequest(String theFile){
		
			  
		//if all is ok, write data to socket
		if(serviceSocket != null && outputStream != null && inputStream != null){
			try{
				try{
					
					//OUTPUT STREAM
					//BufferedImage image = ImageIO.read(new File("dance.jpg"));
					BufferedImage image = ImageIO.read(new File(theFile));
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					ImageIO.write(image, "jpg", byteArrayOutputStream);

					//byte[] imageArray = ByteBuffer.allocate(4).putInt(byteArrayOutputStream.size()).array();
					int imageSize = (int) byteArrayOutputStream.size();

					byte data[] = byteArrayOutputStream.toByteArray();
					//System.out.println("Data sent:"+Arrays.toString(data));
					
					//create and send header	
					byte[] header = SendHeader(1,1,1,2,imageSize);
					System.out.println("File request header sent:" + Arrays.toString(header));
					
					
					//send body
					outputStream.write(data);
					outputStream.flush();
					
					
					//INPUT STREAM
					int messageNumber;
					int numberOfMessages; 
					int[] responseHeader;
					int responseSize;
					byte[] response;
					int i=0, j=0, defaultCounter = 1;
					
					//Loop and get the response headers and body
					for(int k=1; k<=defaultCounter; k++){
						
						responseHeader = GetResponseHeader();
						messageNumber = responseHeader[1];
						numberOfMessages = responseHeader[2];
						System.out.println("Response header:"+ k + Arrays.toString(responseHeader));

						//Very important! Not resetting i to 0 cost me hours!
						
						i=0;
						//read input into byte array
						response = new byte[responseHeader[4]];
						while(i<responseHeader[4]){
							response[i]=inputStream.readByte();
							i++;
						} 
						
						//write byte array to image file
						InputStream in = new ByteArrayInputStream(response);
						BufferedImage t = ImageIO.read(in);
						ImageIO.write(t,"jpg",new File("C:/Users/Absalom/Desktop/Java/apache-tomcat-7.0.70/webapps/dlcloud/results/works.jpg"));
						//ImageIO.write(t,"jpg",new File("C:\\Users\\Absalom\\Desktop\\Java\\uploads\\"));
						//ImageIO.write(t,"jpg",new File("C:\\Users\\Absalom\\Desktop\\Java\\apache-tomcat-7.0.70\\uploads\\"));
						
						//Number of runs is only determined after first run
						defaultCounter = numberOfMessages;
					}
					
					/*
					//NORMAL FILE RECEIVE
					StringBuffer responseData = new StringBuffer();
					String rd;
					try{
						while((rd=inputStream.readLine())!= null){
							responseData.append(rd);
						} 
					}catch (IOException e) {
							System.out.println("Error getting response body:" + e);
					}
					*/
					//try{
						//out = new BufferedOutputStream(new FileOutputStream("im.jpg"));
						//convert the responseData StringBuffer to a byte array
						//out.write(String.valueOf(responseData).getBytes());
						//out.write(image);
						
						//ImageIO.write(image,"jpg",new File("imm.jpg"));
					//}
					//finally{
					//	if(out != null) out.close();
					//}
					
					
				}
				 catch (Exception ex) {
					System.out.println("SendFileRequest: Upload error "+ ex);
				}

				inputStream.close();
				outputStream.close();
				serviceSocket.close();

			}catch (IOException e) {
				System.out.println("SendFileRequest: Failed with error:" + e);
				//e.printStackTrace();
			}
		}
	}
	
	
		
		public  void StopService(){
			
			//if all is ok, write data to socket
			if(serviceSocket != null && outputStream != null && inputStream != null){
				try{
					try{
											
						//create header	
						byte[] header = SendHeader(4,1,1,0,0);

						System.out.println("Host: " + host+" OutputStream: " + outputStream+" InputStream:"+inputStream);
						System.out.println("StopService: Header sent:" + Arrays.toString(header));
						//System.out.println("Server response header: " + Arrays.toString(responseHeader));
						//System.out.println("Result: " + responseBody);
						

					}
					 catch (Exception ex) {
						System.out.println("StopService error "+ ex);
					}

					inputStream.close();
					outputStream.close();
					serviceSocket.close();

				}catch (IOException e) {
					System.out.println("StopService: Connection to server failed with error:" + e);
					//e.printStackTrace();
				}
			}

			
		}
	
	
	
}
