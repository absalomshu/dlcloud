package net.codejava.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import sshconnect.exec;

import java.util.*;

/**
 * Servlet implementation class StartService
 */
@WebServlet(description = "Manage service configurations", urlPatterns = { "/manage" })
public class manage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public manage() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		System.out.println("----------------------------");
		System.out.println("Manage services");
		
		//get GET parameters
		String category = request.getParameter("category");
		
		
		
		//Map apps = new HashMap();
		Map<String, HashMap<String, String>> appList = new HashMap<String, HashMap<String, String>>();
		
		 try {	
			 	File inputFile = new File("C:/Users/Absalom/Desktop/Java/apache-tomcat-7.0.70/webapps/dlcloud/configurations/config.xml");
		         DocumentBuilderFactory dbFactory= DocumentBuilderFactory.newInstance();
		         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		         Document doc = dBuilder.parse(inputFile);
		         doc.getDocumentElement().normalize();
		         //System.out.println("Root element :"+ doc.getDocumentElement().getNodeName());
		         NodeList nList = doc.getElementsByTagName("application");
		         
		         for (int temp = 0; temp < nList.getLength(); temp++) {
		            Node nNode = nList.item(temp);
		            System.out.println("nList.getLength():"+ nList.getLength());
		            //System.out.println("\nCurrent Element :"+ nNode.getNodeName());
		            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		               Element eElement = (Element) nNode;
		               
		               String appId = eElement.getAttribute("id");

		               String inputType = eElement.getElementsByTagName("inputType").item(0).getTextContent();
		               String appName = eElement.getElementsByTagName("name").item(0).getTextContent();
		               String appDescription = eElement.getElementsByTagName("description").item(0).getTextContent();
		               
		               //If the app type matches the catefory selected, add to the list
		             //  if(inputType.equals(category)){
		            	 
		            	   System.out.println("App id : " + appId + " Category: "+category + " InputType: "+  inputType+" (Directory.java)");
			               //System.out.println("Category selected : " + category);
			               
		            	   
		            	   HashMap<String, String> appDetails = new HashMap<String, String>();
		            	   appDetails.put(appName, appDescription);
		           			
		            	   //input appID as key, and name and desc as values
		            	   appList.put(appId, appDetails);
		            	   
		             //  }
		               request.setAttribute("category", category);
		             
		               
		            }
		         }  
		         
		         //System.out.println("Contents of appsList: " + appList);
		    	 request.setAttribute("appList", appList);
			 		
			        
		         
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
		
		 getServletContext().getRequestDispatcher("/manage.jsp").forward(request, response);
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	
	
	
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		
	}
	
	

}
