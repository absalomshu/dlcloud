package net.codejava.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

import communication.Comm;

//for appache commons
import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.*;
import org.apache.commons.fileupload.servlet.*;
import javax.servlet.http.*;
import javax.imageio.ImageIO;
import javax.servlet.*;
import java.net.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import java.nio.*;

import org.json.simple.JSONObject;

@WebServlet("/UploadServlet")
@MultipartConfig(fileSizeThreshold=1024*1024*2,	// 2MB
				 maxFileSize=1024*1024*10,		// 10MB
				 maxRequestSize=1024*1024*50)	// 50MB
public class UploadServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    String serviceHost;
    String servicePort;
    
	/**
	 * Name of the directory where uploaded files will be saved, relative to
	 * the web application directory.
	 */
	private static final String SAVE_DIR = "uploads";

	/**
	 * handles file upload
	 */
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		//APACHE COMMONS
		//Used because it'll be tricky to handle an unspecified number of input files from the jquery with the Servlet 3.0 method
		
		
		
		   File file ;
		   int maxFileSize = 5000 * 1024;
		   int maxMemSize = 5000 * 1024;
		   // gets absolute path of the web application
		   String appPath = request.getServletContext().getRealPath("");
			// constructs path of the directory to save uploaded file
			String savePath = appPath + File.separator + SAVE_DIR;
		   //ServletContext context = pageContext.getServletContext();
		  
			//String filePath = appPath;
			String filePath = "C:\\Users\\Absalom\\Desktop\\Java\\uploads\\";

		   // Verify the content type
		   String contentType = request.getContentType();
		   if ((contentType.indexOf("multipart/form-data") >= 0)) {

		      DiskFileItemFactory factory = new DiskFileItemFactory();
		      // maximum size that will be stored in memory
		      factory.setSizeThreshold(maxMemSize);
		      // Location to save data that is larger than maxMemSize.
		      factory.setRepository(new File("c:\\temp"));

		      // Create a new file upload handler
		      ServletFileUpload upload = new ServletFileUpload(factory);
		      // maximum file size to be uploaded.
		      upload.setSizeMax( maxFileSize );
		      try{ 
		         // Parse the request to get file items.
		         List fileItems = upload.parseRequest(request);

		         // Process the uploaded file items
		         Iterator i = fileItems.iterator();
		         while ( i.hasNext () ) 
		         {
		            FileItem fi = (FileItem)i.next();
		            
		          String ifName;
		          String ifValue;
		           
		          if ( fi.isFormField () ){
		        	  
		        	  //If it's a form field, requet.getParameter() doesn't work with enctype=multipart/...
		            	
		            	   ifName = fi.getFieldName();//text1
		                   ifValue = fi.getString();
		                   System.out.println(ifName+" : "+ ifValue);
		                   //Get the parameters from the form
		                   if (ifName.equals("port")) {
		                	   servicePort = ifValue;
		                   } else if (ifName.equals("host")) {
		                	   serviceHost = ifValue;
		                   }
		            }
		            
		         
		         
		            if ( !fi.isFormField () ){
		            	//If it's not a form field, it's a file. Treat it
						// Get the uploaded file parameters
						//
		            	
		            	
						String fieldName = fi.getFieldName();
						String fileName = fi.getName();
						boolean isInMemory = fi.isInMemory();
						long sizeInBytes = fi.getSize();
						
						
						System.out.println("fieldName.."+fieldName);
						
						//Don't process the intermediate file. Rather, sent it in the init command
						if(fieldName.equals("intermediateFile")){
							//Rather connect and initialize the service here
							Comm client = new Comm(serviceHost, Integer.parseInt(servicePort));
							
							boolean connectionSuccessful;
				            connectionSuccessful = client.ConnectService();
				          //if it doesn't succeed, try again 3ce
				               if(!connectionSuccessful){
					               for(int tries = 2; tries <= 10; tries++){
					            	   try{
						            	   System.out.println("Pass: "+tries+" failed. Retrying in 0.5 seconds(StartService.java)");
						            	   Thread.sleep(500);
						            	   
						            	   connectionSuccessful = client.ConnectService();
						            	   
						            	   //
						            	   if(connectionSuccessful) break;
						            	   
						               }catch(InterruptedException err){
						            	   Thread.currentThread().interrupt();
						               }
					            	   
					               }					               
				               }
							
							
				               if(connectionSuccessful){
									System.out.println("Sending init command...(UploadServlet.java)");
									
									
									// Upload the intermediate file
									if( fileName.lastIndexOf("\\") >= 0 ){
										file = new File( filePath + fileName.substring( fileName.lastIndexOf("\\"))) ;
									}else{
										file = new File( filePath + fileName.substring(fileName.lastIndexOf("\\")+1)) ;
									} 
									fi.write(file) ;
				          
									
									
									String fullFile = filePath + fileName;	
									client.InitServiceWithParameters(fullFile);
									//client.InitService();
				               }else{
				            
					           		HttpSession session = request.getSession(false);
					           		session.setAttribute("notice", "Service failed to start.");
					           		//getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
					           		System.out.println("System should exit now");	
					           		response.sendRedirect("/error.jsp");
					           		//For some reason, redirecting to the error page as above doesn't work.
					           		//However, response.senRedirect and break together somehow trigger the default alert that sth went wrong, so they can t\
					           		//try again.
					           		break;
				               }
							
						}else{ //if it's NOT an intermediate file, it's an input
							//Only process non empty file inputs
							if(sizeInBytes != 0 ){
								System.out.println("FieldName:"+fieldName);
								System.out.println("Filename:"+fileName);
								
								// Write the file
								if( fileName.lastIndexOf("\\") >= 0 ){
									file = new File( filePath + fileName.substring( fileName.lastIndexOf("\\"))) ;
								}else{
									file = new File( filePath + fileName.substring(fileName.lastIndexOf("\\")+1)) ;
								} 
								
								//the above if else works, but keeps returning the "access is denied error"
								//because there's no backslash \ between the filepath and filename
								//hence I've redone it
								
								//file = new File( savePath +"\\"+ fileName) ;
								fi.write(file) ;
			          
								System.out.println("Test:"+file);
								String theFile = filePath + fileName;
								System.out.println("Test:"+filePath +" --- "+fileName);
								System.out.println("UploadServlet.java: Uploaded file: "+ theFile);
								
								String output = CommunicateWithService(serviceHost, servicePort, theFile);
								System.out.println("Output "+ output.toString());
								response.getWriter().write(output);
				            }
						}
	            }
		            
		         }
		      }catch(Exception ex) {
		         System.out.println("UploadServlet: error: " + ex);
		      } 
		   }else{
		      System.out.println("UploadServlet: No multipart data?");
		   }
		
		
		
		
		//DEFAULT SERVLET METHOD
		/*
			// gets absolute path of the web application
			String appPath = request.getServletContext().getRealPath("");
			// constructs path of the directory to save uploaded file
			String savePath = appPath + File.separator + SAVE_DIR;
	
			// creates the save directory if it does not exists
			File fileSaveDir = new File(savePath);
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdir();
			}
			
			try{
				for (Part part : request.getParts()) {
					String fileName = extractFileName(part);
					// refines the fileName in case it is an absolute path
					fileName = new File(fileName).getName();
					
					System.out.println("File name before writing"+fileName);
					
						part.write(savePath + File.separator + fileName);
					
					//ADDITIONAL CODE
					String fileNameAndPath = savePath + File.separator + fileName ;
					
					CommunicateWithService(fileNameAndPath);
					//END OF ADDITIONAL CODE
					
				}
			}catch(Exception ex){
				System.out.println("File write error: " + ex);
			} */

		//request.setAttribute("message", "Upload has been done successfully!");
		//getServletContext().getRequestDispatcher("/sample_message.jsp").forward(request, response);
	}

	/**
	 * Extracts file name from HTTP header content-disposition
	 */
	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length()-1);
			}
		}
		return "";
	}
	
	/**Create interraction sequence with service */
	
	private String CommunicateWithService(String serviceHost, String servicePort, String fileNameAndPath){
		//System.out.println("Initializing communicaiton with service..."); 
		
		ArrayList fullResponse;
		//String responseDataType="";
		String fullTextResponse="";
		String textFullResponse="";
		int servicePortAsInteger = Integer.parseInt(servicePort);
		
		//Json object for result to be sent to browser
		JSONObject resultObj = new JSONObject();
		
		//Init service and send file		
		Comm client = new Comm(serviceHost, servicePortAsInteger);
	
		client.ConnectService();
		System.out.println("Sending file to service...(UploadServlet.java)"); 
		
		fullResponse = client.SendImageRequest(fileNameAndPath);
		
		//get the number of files in the response. It's actually one less than this, since first value is dataType
		int numberOfResults = fullResponse.size();
		
		//Get the response format. 1st value of array list
		int responseDataType = (int)fullResponse.get(0); 
		
		//Set it in json object for browser
		resultObj.put("type", Integer.toString(responseDataType));
		System.out.println("Response data type: (UploadServlet.java)" + responseDataType); 
		
		//Get all results as single string
		for(int k=1; k<numberOfResults; k++){
			//add all as one string
			textFullResponse += fullResponse.get(k); 
			System.out.println("Result" + k +": "+ fullResponse.get(k)); 
			
			//Get result as byte array
			byte[] result =  (byte[])fullResponse.get(k);
			//Convert byte array to string
			
			
			//Build JSON object
			 
			//If it's text, decode
			if(responseDataType==1){
				try {
					
					String TextResponse = new String(result,"UTF-8");
					fullTextResponse += TextResponse;
					System.out.println("Text result:"+ k + TextResponse); 
					
					//Write to result object
					resultObj.put(k, TextResponse);
				
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			//Ift it's an image, base64 encode so it can be displayed in browser
			else if(responseDataType==2){
				
				String base64Encoded = Base64.getEncoder().encodeToString(result);
				resultObj.put(k, base64Encoded);
				//resultObj.put(k, Arrays.toString(result));
			}
			 
			
		}
		
		
		
		
		System.out.println("Result JSON:" + resultObj); 
		System.out.println("Full text response(UploadServlet.java):" + fullTextResponse); 
		System.out.println("Full response(UploadServlet.java):" + fullResponse); 

		
		
		
		/*
		Comm.ConnectService(serviceHost, servicePortAsInteger);
		Comm.InitService();
		Comm.ConnectService(serviceHost, servicePortAsInteger);
		Comm.SendFileRequest(fileNameAndPath);
		*/
		
		
		
		
		
		System.out.println("----------------Finished------------------------"); 
		
		//New line character to separate result groups in interface
		//System.out.println("Result being tested"+resultObj.toString()+",");
		return resultObj.toString()+"\n";
	}
}