package net.codejava.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import communication.Comm;

/**
 * Servlet implementation class StopService
 */
@WebServlet(description = "Stops a particular service", urlPatterns = { "/StopService" })
public class StopService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StopService() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		String host = request.getParameter("host");
		String port = request.getParameter("port");
		int servicePortAsInteger = Integer.parseInt(port);
		
		System.out.println("StopService.java: Trying to stop service: Host: "+host+" Port: "+port); 
		
		Comm client = new Comm(host,servicePortAsInteger);
		client.ConnectService();
		client.StopService();
		
		
		/*System.out.println("Trying to stop service"); 
		Comm.ConnectService("dl-srv1.u-aizu.ac.jp", 10000);
		Comm.StopService(); */
		
		//redirect to home page
		//request.setAttribute("message", "Service stopped!");
		//getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
		//response.sendRedirect("index.html");
		
		//if request is not from HttpServletRequest, you should do a typecast before
		HttpSession session = request.getSession(false);
		//save message in session
		session.setAttribute("notice", "Service stopped.");
		response.sendRedirect(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPosst(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		String host = request.getParameter("host");
		String port = request.getParameter("port");
		int servicePortAsInteger = Integer.parseInt(port);
		
		System.out.println("StopService.java: Trying to stop service: Host: "+host+" Port: "+port); 
		
		Comm client = new Comm(host,servicePortAsInteger);
		client.ConnectService();
		client.StopService();
		
		
		/*System.out.println("Trying to stop service"); 
		Comm.ConnectService("dl-srv1.u-aizu.ac.jp", 10000);
		Comm.StopService(); */
		
		//redirect to home page
		request.setAttribute("message", "Service stopped!");
		getServletContext().getRequestDispatcher("/index.html").forward(
				request, response);
		
	}

}
