package net.codejava.servlet;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.File;
import javax.xml.parsers.*;
import org.w3c.dom.*;

import communication.Comm;
import sshconnect.exec;
import java.net.*;

/**
 * Servlet implementation class StartService
 */
@WebServlet(description = "Launches the service container", urlPatterns = { "/StartService" })
public class StartService extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StartService() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String idOfServiceSelected = request.getParameter("serviceSelected");
		Socket testSocket = null;
		
		 try {	
			  	// File inputFile = new File("C:/Users/Absalom/Desktop/Java/apache-tomcat-7.0.70/webapps/dlcloud/configurations/config.xml");
			  	String rootDir = getServletContext().getInitParameter("root-dir");
			 	File inputFile = new File(rootDir+"configurations/config.xml");
		        
			  	 DocumentBuilderFactory dbFactory= DocumentBuilderFactory.newInstance();
		         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		         Document doc = dBuilder.parse(inputFile);
		         doc.getDocumentElement().normalize();
		         System.out.println("Root element :"+ doc.getDocumentElement().getNodeName());
		         NodeList nList = doc.getElementsByTagName("application");
		         System.out.println("----------------------------");
		         
		         for (int temp = 0; temp < nList.getLength(); temp++) {
		            Node nNode = nList.item(temp);
		            // System.out.println("nList.getLength():"+ nList.getLength());
		           // System.out.println("\nCurrent Element :"+ nNode.getNodeName());
		            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
		               Element eElement = (Element) nNode;
		               
		               String appId = eElement.getAttribute("id");
		              
		               //Get the corresponding configuration
		               
		               if(appId.equals(idOfServiceSelected)){
		            	   System.out.println("App id from config: " + appId);
			               System.out.println("Id of service selected : " + idOfServiceSelected);
			               String serviceStartCommand = eElement.getElementsByTagName("serviceStartCommand").item(0).getTextContent();
			              
			               String name = eElement.getElementsByTagName("name").item(0).getTextContent();
			             
			               String description = eElement.getElementsByTagName("description").item(0).getTextContent();
			               String host = eElement.getElementsByTagName("host").item(0).getTextContent();
			               String inputType = eElement.getElementsByTagName("inputType").item(0).getTextContent();
			               String intermediateFile = eElement.getElementsByTagName("intermediateFile").item(0).getTextContent();
			               String outputType = eElement.getElementsByTagName("outputType").item(0).getTextContent();
			               String port = eElement.getElementsByTagName("port").item(0).getTextContent();
			               String user = eElement.getElementsByTagName("hostUser").item(0).getTextContent();
			               String password = eElement.getElementsByTagName("hostPassword").item(0).getTextContent();
			               
			               
			               System.out.println("Starting service...(StartService.java)");
			               System.out.println("Start service command: "+serviceStartCommand);
			               
			               
			               
			               
			               
			               int servicePortAsInteger = Integer.parseInt(port);
			              
			               try{
			            	   //Firt test that port to see if a service is listenning
			            	   testSocket = new Socket(host,servicePortAsInteger);
			               }catch(Exception e){
			            	   System.out.println("StartService: Service running check error: "+e);
			               }
			               finally{
			            	   //If there's no service running on that port
			            	   if(testSocket==null){
			            		   
			            		   
			            		   
			            		   //Connect to host and start service
					               exec ex = new exec(host, user, password, serviceStartCommand);
					               ex.SshConnect();
					               
					               //If there's no intermediate image, proceed as usual
					               if(!intermediateFile.equalsIgnoreCase("yes")){
					            	   
					            	   System.out.println("There is NO intermediate file");
						               Comm client = new Comm(host, servicePortAsInteger);
							       		
						               boolean connectionSuccessful;
						               connectionSuccessful = client.ConnectService();
						               
						               //if it doesn't succeed, try again 3ce
						               if(!connectionSuccessful){
							               for(int tries = 2; tries <= 50; tries++){
							            	   try{
								            	   System.out.println("Pass: "+tries+" failed. Retrying in 0.5 seconds(StartService.java)");
								            	   Thread.sleep(500);
								            	   
								            	   connectionSuccessful = client.ConnectService();
								            	   
								            	   //
								            	   if(connectionSuccessful) break;
								            	   
								               }catch(InterruptedException err){
								            	   Thread.currentThread().interrupt();
								               }
							            	   
							               }					               
						               }
						               
						               if(connectionSuccessful){
						            	   	System.out.println("Sending init command...(StartService.java)");
								       	 	client.InitService();
								    
								       	 	request.setAttribute("idOfServiceSelected", idOfServiceSelected);
							               request.setAttribute("name", name);
							               request.setAttribute("description", description);
							               request.setAttribute("outputType", name);
							               request.setAttribute("serviceStartCommand", serviceStartCommand);
							               request.setAttribute("host", host);
							               request.setAttribute("inputType", inputType);
							               request.setAttribute("intermediateFile", intermediateFile);
							               request.setAttribute("outputType", outputType);
							               request.setAttribute("port", port);
							               request.setAttribute("notice", "Service started!");
							               getServletContext().getRequestDispatcher("/newworkarea.jsp").forward(request, response);
									 		
						               }else{
						            	   	//println("Sending init command...(StartService.java)");
						            	   	// request.setAttribute("notice", "Service failed to start!");
									 		//getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
						            	   	//if request is not from HttpServletRequest, you should do a typecast before
							           		HttpSession session = request.getSession(false);
							           		//save message in session
							           		session.setAttribute("notice", "Service failed to start.");
							           		//this redirects home
							           		//response.sendRedirect(request.getContextPath());
							           		getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
							           		
					           		
						               }
				               
			            	   
					               } //END IF NO INTERMEDIATE IMAGE
					               //ELSE IF THERE IS AN INTERMEDIATE FILE
					               else{
							       		System.out.println("Intermediate file exists. Init command not sent...");	
							       		request.setAttribute("idOfServiceSelected", idOfServiceSelected);
							            request.setAttribute("name", name);
							            request.setAttribute("description", description);
							            request.setAttribute("outputType", name);
							            request.setAttribute("serviceStartCommand", serviceStartCommand);
							            request.setAttribute("host", host);
							            request.setAttribute("inputType", inputType);
							            request.setAttribute("intermediateFile", intermediateFile);
							            request.setAttribute("outputType", outputType);
							            request.setAttribute("port", port);
							            request.setAttribute("notice", "Service started!");
							            getServletContext().getRequestDispatcher("/newworkarea.jsp").forward(request, response);
							       	}
					               
					               
			            	   
			            	   }else{
				            	   HttpSession session = request.getSession(false);
				            	   session.setAttribute("notice", "Another service is running on same port.");
				            	 //this redirects home
				            	   getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
				               }
			               }
			               
			               
			               
			            }
		            }
		         }  
		         
		       //redirect to home page
			 		
			         
			        
		         
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		
		/*
		
		
		
		System.out.println("Trying to start service");
		
		//get POST parameters
		String serviceSelected = request.getParameter("serviceSelected");
		//call the main function
		//String[] args = {};
		//exec.main(args);
		
		
		
		
		//get corresponding config file
		String XmlPath = serviceSelected + ".xml";
		
		System.out.println("Service selected: "+XmlPath);
		
		
		 try {	
			 //Get the corresponding config
	         File inputFile = new File("C:/Users/Absalom/Desktop/Java/apache-tomcat-7.0.70/webapps/dlcloud/configurations/"+XmlPath);
	         System.out.println(inputFile);
	         
	         
	         DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	         DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	         Document doc = dBuilder.parse(inputFile);
	         doc.getDocumentElement().normalize();
	         System.out.println("Root element :"+ doc.getDocumentElement().getNodeName());
	        // System.out.println("Root element :"+ doc.getAttribute("attributeName");
	         
	         
	         
	         
	         NodeList nList = doc.getElementsByTagName("appConfiguration");
	         
	         
	         for (int temp = 0; temp < nList.getLength(); temp++) {
	            Node nNode = nList.item(temp);
	            System.out.println("\nCurrent Element :" + nNode.getNodeName());
	            
	            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	               Element eElement = (Element) nNode;
	               
	              // System.out.println("Student roll no : " + eElement.getAttribute("title"));
	               String serviceStartCommand = eElement.getElementsByTagName("serviceStartCommand").item(0).getTextContent();
	               String title = eElement.getElementsByTagName("title").item(0).getTextContent();
	               String name = eElement.getElementsByTagName("name").item(0).getTextContent();
	               String application = eElement.getElementsByTagName("application").item(0).getTextContent();
	               String description = eElement.getElementsByTagName("description").item(0).getTextContent();
	               String host = eElement.getElementsByTagName("host").item(0).getTextContent();
	               String inputType = eElement.getElementsByTagName("inputType").item(0).getTextContent();
	               String intermediateFile = eElement.getElementsByTagName("intermediateFile").item(0).getTextContent();
	               String outputType = eElement.getElementsByTagName("outputType").item(0).getTextContent();
	               String port = eElement.getElementsByTagName("port").item(0).getTextContent();
	               String user = "service";
	               String password = "service";
	               
	               
	               
	               System.out.println(serviceStartCommand);
	               exec ex = new exec();
	               ex.SshConnect(host, user, password, serviceStartCommand);
	               System.out.println("----------------------------");
	               
	               request.setAttribute("name", name);
	               request.setAttribute("description", description);
	               request.setAttribute("outputType", name);
	               request.setAttribute("serviceStartCommand", serviceStartCommand);
	               request.setAttribute("title", title);
	               request.setAttribute("application", application);
	               request.setAttribute("host", host);
	               request.setAttribute("inputType", inputType);
	               request.setAttribute("intermediateFile", intermediateFile);
	               request.setAttribute("outputType", outputType);
	               request.setAttribute("port", port);
	              
	            }
	         }
	         
	       //redirect to home page
	 		request.setAttribute("message", "Service started!");
	 		getServletContext().getRequestDispatcher("/newworkarea.jsp").forward(request, response);
	         
	         
	         
	      } catch (Exception e) {
	         e.printStackTrace();
	      } */
	}
	
	

}
