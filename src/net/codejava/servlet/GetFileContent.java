package net.codejava.servlet;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import communication.Comm;

//for appache commons
import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.*;
import org.apache.commons.fileupload.servlet.*;
import javax.servlet.http.*;
import javax.servlet.*;
import java.net.*;
import java.io.*;
import java.util.*;
import java.nio.*;



@WebServlet("/GetFileContent")
@MultipartConfig(fileSizeThreshold=1024*1024*2,	// 2MB
				 maxFileSize=1024*1024*10,		// 10MB
				 maxRequestSize=1024*1024*50)	// 50MB
public class GetFileContent extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    String serviceHost;
    String servicePort;
    protected String inputText;
    int startProcessing;
    
    //This form first uploads a file to get it's content and also to send for processing.
    //The startProcessing variable determines which to do.
    
    
	/**
	 * Name of the directory where uploaded files will be saved, relative to
	 * the web application directory.
	 */
	private static final String SAVE_DIR = "\\uploads\\";

	/**
	 * handles file upload
	 */
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		//APACHE COMMONS
		//Used because it'll be tricky to handle an unspecified number of input files from the jquery with the Servlet 3.0 method
		
		
		
		   File file ;
		   int maxFileSize = 5000 * 1024;
		   int maxMemSize = 5000 * 1024;
		   // gets absolute path of the web application
		   String appPath = request.getServletContext().getRealPath("");
			// constructs path of the directory to save uploaded file
			String savePath = appPath + File.separator + SAVE_DIR;
		   //ServletContext context = pageContext.getServletContext();
		  
			String filePath = savePath;
			//String filePath = "C:\\Users\\Absalom\\Desktop\\Java\\uploads\\";

		   // Verify the content type
		   String contentType = request.getContentType();
		   if ((contentType.indexOf("multipart/form-data") >= 0)) {

		      DiskFileItemFactory factory = new DiskFileItemFactory();
		      // maximum size that will be stored in memory
		      factory.setSizeThreshold(maxMemSize);
		      // Location to save data that is larger than maxMemSize.
		      factory.setRepository(new File("c:\\temp"));

		      // Create a new file upload handler
		      ServletFileUpload upload = new ServletFileUpload(factory);
		      // maximum file size to be uploaded.
		      upload.setSizeMax( maxFileSize );
		      try{ 
		         // Parse the request to get file items.
		         List fileItems = upload.parseRequest(request);

		         // Process the uploaded file items
		         Iterator i = fileItems.iterator();
		         while ( i.hasNext () ) 
		         {
		            FileItem fi = (FileItem)i.next();
		            
		          String ifName;
		          String ifValue;
		          
		          if ( fi.isFormField () ){
		        	  
		        	  //If it's a form field, requet.getParameter() doesn't work with enctype=multipart/...
		            	
		            	   ifName = fi.getFieldName();//text1
		                   ifValue = fi.getString();
		                   System.out.println(ifName+" : "+ ifValue);
		                   //Get the parameters from the form
		                   if (ifName.equals("port")) {
		                	   servicePort = ifValue;
		                   } else if (ifName.equals("host")) {
		                	   serviceHost = ifValue;
		                   }else if (ifName.equals("start_processing")) {
		                	   startProcessing = Integer.parseInt(ifValue);
		                   }else if (ifName.equals("inputtext")) {
		                	   inputText = ifValue;
		                	   
		                	   
		                	      int startProcessingAsInteger = startProcessing;
		      					if(startProcessingAsInteger==1){
		      						
		      						//NO NEED TO READ FILE AGAIN. JUST GET TEXTAREA
		      						/*File myFile = new File(theFile);
		      						BufferedInputStream in = new BufferedInputStream(new FileInputStream(myFile));
		      						ByteArrayOutputStream baos = new ByteArrayOutputStream();
		      						
		      						int size = in.available();
		      						int count;
		      						
		      						//convert to int. default if long
		      						int inputFileSize = (int) myFile.length();
		      						byte[] data = new byte[(int) myFile.length()];
		      						
		      						
		      						while ((count = in.read(data)) > 0) {
		      							baos.write(data, 0, count);
		      						}
		      						
		      						String textInput = baos.toString(); */
		      						
		      						String output = CommunicateWithService(serviceHost, servicePort, inputText);
		      						response.getWriter().write(output);
		      					}  
		      		            
		                	   
		                	   
		                	   
		                   }
		            }
		            
		         
		          
		            if ( !fi.isFormField () ){
		            	//If it's not a form field, it's a file. Treat it
						// Get the uploaded file parameters
						//
		            	
		            	
						String fieldName = fi.getFieldName();
						String fileName = fi.getName();
						boolean isInMemory = fi.isInMemory();
						long sizeInBytes = fi.getSize();
						
						//Only process non empty file inputs
						if(sizeInBytes != 0 ){
							System.out.println("Filename:"+fileName);
							
							// Write the file
							if( fileName.lastIndexOf("\\") >= 0 ){
								file = new File( filePath + fileName.substring( fileName.lastIndexOf("\\"))) ;
							}else{
								file = new File( filePath + fileName.substring(fileName.lastIndexOf("\\")+1)) ;
							} 
							
							//the above if else works, but keeps returning the "access is denied error"
							//because there's no backslash \ between the filepath and filename
							//hence I've redone it
							
							//file = new File( savePath +"\\"+ fileName) ;
							fi.write(file) ;
		          
							System.out.println("Test:"+file);
							String theFile = filePath + fileName;
							System.out.println("Test:"+filePath +" --- "+fileName);
							System.out.println("UploadServlet.java: Uploaded file: "+ theFile);
							
							
							
							
							
			            }
		            }
		            
		            
		            
		            
		            
		            
		      
		            
		            
		            
		            
		            
		            
		            
		         }
		      }catch(Exception ex) {
		         System.out.println("GetFileContent: error: " + ex);
		      } 
		   }else{
		      System.out.println("GetFileContent: No multipart data?");
		   }
		
		
		
		
		//DEFAULT SERVLET METHOD
		/*
			// gets absolute path of the web application
			String appPath = request.getServletContext().getRealPath("");
			// constructs path of the directory to save uploaded file
			String savePath = appPath + File.separator + SAVE_DIR;
	
			// creates the save directory if it does not exists
			File fileSaveDir = new File(savePath);
			if (!fileSaveDir.exists()) {
				fileSaveDir.mkdir();
			}
			
			try{
				for (Part part : request.getParts()) {
					String fileName = extractFileName(part);
					// refines the fileName in case it is an absolute path
					fileName = new File(fileName).getName();
					
					System.out.println("File name before writing"+fileName);
					
						part.write(savePath + File.separator + fileName);
					
					//ADDITIONAL CODE
					String fileNameAndPath = savePath + File.separator + fileName ;
					
					CommunicateWithService(fileNameAndPath);
					//END OF ADDITIONAL CODE
					
				}
			}catch(Exception ex){
				System.out.println("File write error: " + ex);
			} */
		   
		//request.setAttribute("message", "Upload has been done successfully!");
		//getServletContext().getRequestDispatcher("/sample_message.jsp").forward(
		//		request, response);
	}

	/**
	 * Extracts file name from HTTP header content-disposition
	 */
	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length()-1);
			}
		}
		return "";
	}
	
	/**Create interraction sequence with service */
	
	private String CommunicateWithService(String serviceHost, String servicePort, String textInput){
		System.out.println("Initializing communicaiton with service..."); 
		
		//String number = "10";
		int servicePortAsInteger = Integer.parseInt(servicePort);
		
		//Init service and send file
		//System.out.println("Host and port:"+serviceHost+servicePort); 
		//Comm.ConnectService("dl-srv1.u-aizu.ac.jp", 10010);
		//Comm.ConnectService("163.143.88.162", 10000);
		
		Comm client = new Comm(serviceHost, servicePortAsInteger);
		//client.host = serviceHost;
		//client.port = servicePortAsInteger;
		
		client.ConnectService();
		client.InitService();
		client.ConnectService();
		System.out.println("Sending file to service..."); 
		StringBuffer output = client.SendTextRequest(textInput);
		
		System.out.println("Response from SendTextRequest"+output); 
		System.out.println("----------------Finished------------------------"); 
		//Send result
		return output.toString();
		
		
		
		
		
		
		//return "";
	}
}